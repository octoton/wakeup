package com.octoton.wakeup.Adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmViewModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.Profile.ProfileFactory;
import com.octoton.wakeup.Profile.ProfileTemplate.TestingProfileTemplate;
import com.octoton.wakeup.R;

public class TestAlarmAdapter extends BaseAdapter {
    String msg = "Test Adapter : ";
    private final Context context;
    private final AppCompatActivity activity;
    private ArrayList<Item> items;

    private final Executor executor;
    AlarmModel alarmModel;
    ProfileModel profileModel;

    boolean hasBeenRemove;

    AlarmViewModel viewModel;
    ProfileFactory profileFactory;
    TestingProfileTemplate testingProfile;

    public TestAlarmAdapter(Context context, AppCompatActivity activity, ArrayList<Item> items, ActivityResultLauncher<Intent> directoryPickerLauncher) {
        this.context = context;
        this.activity = activity;
        this.items = items;

        this.executor = Executors.newSingleThreadExecutor();
        alarmModel = null;

        profileFactory = new ProfileFactory(activity, directoryPickerLauncher);
        testingProfile = new TestingProfileTemplate();

        viewModel = new ViewModelProvider(activity).get(AlarmViewModel.class);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.test_alarm_view, container, false);
        }

        Button activation = (Button) convertView.findViewById(R.id.activation);
        Button reload = (Button) convertView.findViewById(R.id.reload);

        executor.execute(() -> {
            alarmModel = AlarmDatabase.getDatabase(context).alarmModel().getTestAlarmItemSynchronous();

            profileModel = profileFactory.getDatabaseVersionOfProfile(testingProfile);
            if (profileFactory.doesProfileNeedToBeUpdated(testingProfile)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reload.setVisibility(View.VISIBLE);
                    }
                });
            }
            if (!profileFactory.doesProfileAlreadyExist(testingProfile)) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activation.setVisibility(View.GONE);
                    }
                });
            }
        });
        hasBeenRemove = false;

        reload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                executor.execute(() -> {
                    profileModel = profileFactory.getLastVersionOfProfile(testingProfile);
                });
                activation.setVisibility(View.VISIBLE);
                reload.setVisibility(View.GONE);
            }
        });

        activation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createNewAlarm(viewModel);
            }
        });

        Button deletion = (Button) convertView.findViewById(R.id.deletion);

        deletion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (alarmModel != null) {
                    viewModel.deleteItem(context, alarmModel);
                    hasBeenRemove = true;
                }
            }
        });

        return convertView;
    }

    private void createNewAlarm(AlarmViewModel viewModel) {

        Calendar cal = getTargetTime();
        boolean firstTime = false;
        if (alarmModel == null || hasBeenRemove) {
            hasBeenRemove = false;
            firstTime = true;
            // TODO: only ask user if a alarm is activated, so when adding one but also when opening the app (see MainActivity)
            NotificationManager.manageNotificationPermission(activity);

            // name and days should be ask to the user not set in stone
            alarmModel = new AlarmModel("Testing", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), 0, true);
            alarmModel.setProfile(profileModel);
        }
        alarmModel.isTesting = true;

        if (firstTime) {
            viewModel.addItemForTesting(activity, alarmModel, cal);
        } else {
            alarmModel.setHour(cal.get(Calendar.HOUR_OF_DAY));
            alarmModel.setMinute(cal.get(Calendar.MINUTE));
            viewModel.updateItemForTesting(activity, alarmModel, cal);
        }
    }

    private Calendar getTargetTime() {
        Calendar calNow = Calendar.getInstance();
        Calendar calSet = (Calendar) calNow.clone();

        calSet.add(Calendar.SECOND, AlarmModel.testingSecondDelay);
        /*
        calSet.set(Calendar.DAY_OF_MONTH, sDay);
        calSet.set(Calendar.HOUR_OF_DAY, 16);
        calSet.set(Calendar.MINUTE, 59);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);*/

        return calSet;
    }

    public static class Item {
        private String title;
        private boolean isActivated;

        public Item(String title, boolean isActivated) {
            this.title = title;
            this.isActivated = isActivated;
        }

        public String getTitle() {
            return this.title;
        }

        public void setActivation(boolean isActivated) {
            this.isActivated = isActivated;
        }
    }
}
