package com.octoton.wakeup.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmViewModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.Profile.ProfileFactory;
import com.octoton.wakeup.Profile.ProfileTemplate.ProfileTemplate;
import com.octoton.wakeup.R;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ProfileViewAdapter extends ArrayAdapter<ProfileModel> {
    private final AppCompatActivity context;
    private final AlarmModel alarmModel;
    private final AlarmViewModel alarmViewModel;
    private final AlertDialog dialog;
    private final ProfileFactory profileFactory;
    private final Executor executor;

    public ProfileViewAdapter(AppCompatActivity activity, List<ProfileModel> items, AlarmModel alarmModel, AlarmViewModel alarmViewModel, AlertDialog dialog, ActivityResultLauncher<Intent> directoryPickerLauncher) {
        super(activity, 0, items);
        this.context = activity;

        this.alarmModel = alarmModel;
        this.alarmViewModel = alarmViewModel;

        this.dialog = dialog;

        this.profileFactory = new ProfileFactory(activity, directoryPickerLauncher);
        this.executor = Executors.newSingleThreadExecutor();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.dialog_profile_view, parent, false);
        }

        final ProfileModel profile = getItem(position);

        TextView name = convertView.findViewById(R.id.name);
        Button reload = convertView.findViewById(R.id.reload);

        if (profile != null) {
            name.setText(profile.getName());

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alarmModel.setProfile(profile);
                    alarmViewModel.updateItem(context, alarmModel);

                    dialog.dismiss();
                }
            });

            ProfileTemplate template = ProfileFactory.getProfileTemplate(profile);
            reload.setVisibility(View.INVISIBLE);
            if (template != null) {
                executor.execute(() -> {
                    if (profileFactory.doesProfileNeedToBeUpdated(template)) {
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                reload.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });

                reload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reload.setVisibility(View.INVISIBLE);

                        executor.execute(() -> {
                            profileFactory.getLastVersionOfProfile(template);
                        });
                    }
                });
            }
        }

        return convertView;
    }
}
