package com.octoton.wakeup.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.BootReceiver;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmViewModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.Profile.ProfileFactory;
import com.octoton.wakeup.Profile.ProfileTemplate.ProfileTemplate;
import com.octoton.wakeup.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.RecyclerViewHolder> {
    String msg = "Alarm Adapter : ";
    private final AppCompatActivity context;
    private List<AlarmModel> alarmModelList;
    private AlarmViewModel alarmViewModel;
    private ActivityResultLauncher<Intent> directoryPickerLauncher;
    private ProfileFactory profileFactory;

    public AlarmAdapter(AppCompatActivity activity, List<AlarmModel> alarmModelList, AlarmViewModel alarmViewModel, ActivityResultLauncher<Intent> directoryPickerLauncher) {
        this.context = activity;
        this.alarmModelList = alarmModelList;
        this.alarmViewModel = alarmViewModel;
        this.directoryPickerLauncher = directoryPickerLauncher;
        this.profileFactory = new ProfileFactory(activity, directoryPickerLauncher);

        setHasStableIds(true);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_view, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        // TODO: when click on stuff, update of view should be called to not have to wait sys-db update (a little slow - really visible on debug mode)
        AlarmModel alarmModel = alarmModelList.get(position);

        holder.clock.setOnClickListener(null);
        holder.chip.setOnClickListener(null);
        holder.edit_title.setOnEditorActionListener(null);
        holder.edit_title.setOnFocusChangeListener(null);
        holder.expand.setOnClickListener(null);
        holder.profil_and_weekday.setOnClickListener(null);
        holder.remove.setOnClickListener(null);
        holder.isActivated.setOnCheckedChangeListener(null);

        holder.title.setText(alarmModel.getName());
        holder.isActivated.setChecked(alarmModel.getIsActivated());

        if (alarmModel.getProfile() != null)
            holder.chip.setText(alarmModel.getProfile().getName());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, alarmModel.getHour());
        calendar.set(Calendar.MINUTE, alarmModel.getMinute());
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        String clockText = timeFormat.format(calendar.getTime());
        holder.clock.setText(clockText);

        setAlarmStatus(holder, alarmModel);
        setPauseOrProgram(holder, alarmModel);

        if (alarmModel.getIsActivated() && alarmModel.hasPreviewAlreadyBeenShown()) // && !alarmModel.hasPreviewBeenDismiss())
            holder.ignoreAlarm.setVisibility(View.VISIBLE);
        else
            holder.ignoreAlarm.setVisibility(View.GONE);

        holder.ignoreAlarm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cancelIntent = new Intent(context, AlarmReceiver.class);
                cancelIntent.setAction(AlarmReceiver.STOP_ALARM_FROM_PREVIEW);
                cancelIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, alarmModel.id);
                PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(alarmModel.id, 0, NotificationManager.ACTION.PreviewStop.ordinal()), cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
                try {
                    cancelPendingIntent.send();
                } catch (PendingIntent.CanceledException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        if (alarmModel.hasItBeenIgnored()) {
            holder.alarmHasBeenIgnored.setVisibility(View.VISIBLE);

            DateFormat format = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault());
            holder.alarmHasBeenIgnored.setText("Alarm has been ignored (next one on "+ format.format(alarmModel.getNextCalendar().getTime()) + ")");
        } else
            holder.alarmHasBeenIgnored.setVisibility(View.GONE);

        if (alarmModel.hasBeenSnoozed())
            holder.snoozedAlarm.setVisibility(View.VISIBLE);
        else
            holder.snoozedAlarm.setVisibility(View.GONE);

        holder.weekday_selector.removeAllViews();
        int day = calendar.getFirstDayOfWeek();
        for (int i = 0; i < AlarmModel.DAY.values().length; i++) {
            boolean dayActivated = alarmModel.isDayActivated(alarmModel.convertDay(day));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;

            MaterialCheckBox checkBox = new MaterialCheckBox(context);
            checkBox.setId(View.generateViewId());
            checkBox.setTag(alarmModel.convertDay(day));
            checkBox.setChecked(dayActivated);

            checkBox.setLayoutParams(params);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    alarmModel.setDay((AlarmModel.DAY) buttonView.getTag(), isChecked);
                    alarmViewModel.updateItem(context, alarmModel);

                    setPauseOrProgram(holder, alarmModel);
                }
            });

            TextView textView = new TextView(context);
            calendar.set(Calendar.DAY_OF_WEEK, day);
            String dayString = new SimpleDateFormat("EE").format(calendar.getTime());
            dayString = dayString.substring(0, 1).toUpperCase() + dayString.substring(1).toLowerCase();
            textView.setText(dayString);
            textView.setLayoutParams(params);

            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.addView(checkBox);
            layout.addView(textView);

            holder.weekday_selector.addView(layout);
            day = AlarmModel.getNextDay(day);
        }
        holder.isActivated.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                alarmModel.setIsActivated(isChecked);
                alarmViewModel.updateItem(context, alarmModel);
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alarmViewModel.deleteItem(context, alarmModel);

                alarmModelList.remove(position);
                notifyItemRemoved(position);
            }
        });
        holder.profil_and_weekday.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toggleMoreOption(holder, alarmModel);
            }
        });
        holder.expand.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toggleMoreOption(holder, alarmModel);
            }
        });
        holder.edit_title.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    alarmModel.setName(((TextInputEditText)v).getText().toString());
                    alarmViewModel.updateItem(context, alarmModel);
                    holder.edit_title.clearFocus();
                    return false;
                }
                return false;
            }
        });
        holder.edit_title.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    alarmModel.setName(((TextInputEditText)v).getText().toString());
                    alarmViewModel.updateItem(context, alarmModel);
                }
            }
        });

        holder.chip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(() -> {
                    List<ProfileModel> allProfiles = AlarmDatabase.getDatabase(context).profileModel().getAllProfileSynchronous();

                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
                            builder.setTitle("Profile list");

                            View customView = LayoutInflater.from(context).inflate(R.layout.dialog_profiles, null);
                            builder.setView(customView);
                            AlertDialog dialog = builder.create();

                            customView.findViewById(R.id.add_templates).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    executor.execute(() -> {
                                        List<ProfileTemplate> templates = ProfileFactory.getAllProfileTemplate();
                                        for (ProfileTemplate template : templates) {
                                            profileFactory.getLastVersionOfProfile(template);
                                        }
                                        dialog.dismiss();
                                    });
                                }
                            });

                            ListView listView = customView.findViewById(R.id.listView);
                            ProfileViewAdapter adapter = new ProfileViewAdapter(context, allProfiles, alarmModel, alarmViewModel, dialog, directoryPickerLauncher);
                            listView.setAdapter(adapter);

                            dialog.show();
                        }
                    });
                });
            }
        });

        holder.clock.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int hour = alarmModel.getHour();
                int minute = alarmModel.getMinute();
                int timeFormat = TimeFormat.CLOCK_24H;
                if (!android.text.format.DateFormat.is24HourFormat(v.getContext())) {
                    timeFormat = TimeFormat.CLOCK_12H;
                }
                MaterialTimePicker materialTimePicker = new MaterialTimePicker.Builder()
                        .setTimeFormat(timeFormat)
                        .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                        .setHour(hour)
                        .setMinute(minute)
                        .build();
                materialTimePicker.addOnPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // name and days should be ask to the user not set in stone
                        int selectedHour = materialTimePicker.getHour();
                        int selectedMinute = materialTimePicker.getMinute();
                        alarmModel.setHour(selectedHour);
                        alarmModel.setMinute(selectedMinute);
                        alarmModel.setIsActivated(true);
                        alarmViewModel.updateItem(context, alarmModel);
                    }
                });
                materialTimePicker.show(((AppCompatActivity) context).getSupportFragmentManager(), "alarm_timepicker");
            }
        });

        setInactiveColorIfNeeded(holder, alarmModel);
    }

    private void setInactiveColorIfNeeded(final RecyclerViewHolder holder, AlarmModel alarmModel) {
        float alpha;
        if (holder.more_action.getVisibility() != View.GONE || alarmModel.getIsActivated()) {
            alpha = 1f;
        } else {
            alpha = 0.38f;
        }
        setInactiveColor(holder, alpha);
    }

    private void setInactiveColor(final RecyclerViewHolder holder, float alpha) {
        holder.title.setAlpha(alpha);
        holder.clock.setAlpha(alpha);
        holder.chip.setAlpha(alpha);
        holder.alarm_status.setAlpha(alpha);
    }

    private void setAlarmStatus(final RecyclerViewHolder holder, AlarmModel alarmModel) {
        String status = "";
        Set<AlarmModel.DAY> weekdays = EnumSet.allOf(AlarmModel.DAY.class);
        // TODO: missing text="24 Oct. 2023" when scheduling
        if (alarmModel.isThereLooping()) {
            Calendar calendar = Calendar.getInstance();
            int day = calendar.getFirstDayOfWeek();
            for (int i = 0; i < AlarmModel.DAY.values().length; i++) {
                boolean dayActivated = alarmModel.isDayActivated(alarmModel.convertDay(day));
                if (dayActivated) {
                    status += "\u2B24 "; // activated
                } else {
                    status += "\u25EF "; // not activated
                }
                day = AlarmModel.getNextDay(day);
            }
        } else {
            if (alarmModel.getIsActivated()) {
                Calendar calNow = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, alarmModel.getHour());
                calendar.set(Calendar.MINUTE, alarmModel.getMinute());
                calendar.set(Calendar.SECOND, 0);
                if (calNow.after(calendar)) {
                    status = context.getResources().getString(R.string.tomorrow);
                } else {
                    status = context.getResources().getString(R.string.today);
                }
            } else {
                status = context.getResources().getString(R.string.inactive);
            }
        }
        holder.alarm_status.setText(status);
    }

    private void setPauseOrProgram(final RecyclerViewHolder holder, AlarmModel alarmModel) {
        if (alarmModel.isThereLooping()) {
            holder.pause_alarm.setVisibility(View.VISIBLE);
            holder.program_alarm.setVisibility(View.GONE);
        } else {
            holder.pause_alarm.setVisibility(View.GONE);
            holder.program_alarm.setVisibility(View.VISIBLE);
        }
        //holder.more_action.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
    }

    private int getSetVisibleAndReturnFutureHeight(View view) {
        view.setVisibility(View.VISIBLE);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = 1;
        view.setLayoutParams(layoutParams);

        view.measure(View.MeasureSpec.makeMeasureSpec(Resources.getSystem().getDisplayMetrics().widthPixels,
                        View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED));

        return view.getMeasuredHeight();
    }

    private void rotateIcon(MaterialButton button, float degrees) {
        Drawable drawable = button.getIcon();
        if (drawable != null) {
            ImageView iconView = new ImageView(context);
            iconView.setImageDrawable(rotateDrawable(drawable, degrees));
            button.setIcon(iconView.getDrawable());
        }
    }

    private Drawable rotateDrawable(Drawable drawable, float degrees) {
        final Drawable[] arD = { drawable };
        return new LayerDrawable(arD) {
            @Override
            public void draw(final Canvas canvas) {
                canvas.save();
                canvas.rotate(degrees, drawable.getBounds().width() / 2f, drawable.getBounds().height() / 2f);
                super.draw(canvas);
                canvas.restore();
            }
        };
    }
    private void toggleMoreOption(final RecyclerViewHolder holder, AlarmModel alarmModel) {
        float deg;

        if(holder.more_action.getVisibility() == View.GONE) {
            deg = 180F;

            int height = getSetVisibleAndReturnFutureHeight(holder.more_action);
            holder.more_action.clearAnimation();
            holder.more_action.setAlpha(0f);
            holder.more_action.animate().alpha(1f)
                    .setDuration(200)
                    .setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(@NonNull ValueAnimator valueAnimator) {
                            holder.more_action.getLayoutParams().height = (int)((float)height * valueAnimator.getAnimatedFraction());
                            holder.more_action.requestLayout();
                        }
                    })
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });

            String existingTitle = holder.title.getText().toString();
            holder.edit_title.setText(existingTitle);
            holder.edit_title.setVisibility(View.VISIBLE);
            holder.title.setVisibility(View.GONE);

            holder.chip.setCheckable(true);
            holder.chip.setChecked(true);
            holder.chip.setCheckable(false);

            setInactiveColorIfNeeded(holder, alarmModel);
        } else {
            deg = 0F;

            int height = holder.more_action.getMeasuredHeight();
            holder.more_action.clearAnimation();
            holder.more_action.setAlpha(1f);
            holder.more_action.animate().alpha(0f)
                    .setDuration(200)
                    .setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(@NonNull ValueAnimator valueAnimator) {
                            holder.more_action.getLayoutParams().height = (int)((float)height * (1f - valueAnimator.getAnimatedFraction()));
                            holder.more_action.requestLayout();
                        }
                    })
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            holder.more_action.setVisibility(View.GONE);
                            setInactiveColorIfNeeded(holder, alarmModel);
                        }
                    });

            String newTitle = holder.edit_title.getText().toString();
            holder.title.setText(newTitle);
            holder.edit_title.setVisibility(View.GONE);
            if (!newTitle.isEmpty())
                holder.title.setVisibility(View.VISIBLE);
            else
                holder.title.setVisibility(View.INVISIBLE);

            holder.chip.setCheckable(true);
            holder.chipGroup.clearCheck();
            holder.chip.setCheckable(false);
        }
        //rotateIcon(holder.expand, deg);
        holder.expand.animate().rotation(deg).setDuration(200);
    }

    @Override
    public int getItemCount() {
        return alarmModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return alarmModelList.get(position).id;
    }

    public void changeItems(List<AlarmModel> alarmModelList) {
        Log.d(msg, "add items");

        this.alarmModelList = alarmModelList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextInputEditText edit_title;
        private SwitchCompat isActivated;
        private Button remove;
        private TextView clock;
        private TextView alarm_status;
        private LinearLayout weekday_selector;
        private LinearLayout profil_and_weekday;
        private MaterialButton expand;
        private Chip chip;
        private ChipGroup chipGroup;
        private LinearLayout more_action;
        private TextView pause_alarm;
        private TextView program_alarm;

        private TextView ignoreAlarm;
        private TextView alarmHasBeenIgnored;
        private TextView snoozedAlarm;

        RecyclerViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            edit_title = (TextInputEditText) view.findViewById(R.id.edit_title);
            isActivated = (SwitchCompat) view.findViewById(R.id.activation);
            remove = (Button) view.findViewById(R.id.remove_alarm);
            clock = (TextView) view.findViewById(R.id.clock);
            alarm_status = (TextView) view.findViewById(R.id.alarm_status);
            weekday_selector = (LinearLayout) view.findViewById(R.id.weekday_selector);
            profil_and_weekday = (LinearLayout) view.findViewById(R.id.profile_and_weekday);
            expand = (MaterialButton) view.findViewById(R.id.expand);
            more_action = (LinearLayout) view.findViewById(R.id.more_action);
            chip = (Chip) view.findViewById(R.id.chip);
            chipGroup = (ChipGroup) view.findViewById(R.id.chipGroup);
            pause_alarm = (TextView) view.findViewById(R.id.pause_alarm);
            program_alarm = (TextView) view.findViewById(R.id.program_alarm);
            ignoreAlarm = (TextView) view.findViewById(R.id.ignoreAlarm);
            alarmHasBeenIgnored = (TextView) view.findViewById(R.id.alarmHasBeenIgnored);
            snoozedAlarm = (TextView) view.findViewById(R.id.snoozedAlarm);
        }
    }
}
