package com.octoton.wakeup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.WindowCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.octoton.wakeup.Adapter.TestAlarmAdapter;
import com.octoton.wakeup.Adapter.TestAlarmAdapter.Item;
import com.octoton.wakeup.Adapter.AlarmAdapter;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmModelDao;
import com.octoton.wakeup.Database.AlarmViewModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.Preference.SettingsActivity;
import com.octoton.wakeup.Profile.ProfileFactory;
import com.octoton.wakeup.Profile.ProfileTemplate.DefaultProfileTemplate;


public class MainActivity extends AppCompatActivity {
    String msg = "Main : ";

    private Menu menu;
    private boolean profileAreLoaded = false;

    /* Manage menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar, menu);
        this.menu = menu;

        if (profileAreLoaded)
            menu.findItem(R.id.action_alarm).setVisible(true);

        return true;
    }

    AlarmViewModel viewModel;
    ProfileModel profileModel;
    ProfileFactory profileFactory;
    DefaultProfileTemplate defaultProfile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_alarm:
                addAlarm(this, viewModel);
                return true;
            case R.id.action_settings:
                Intent myIntent = new Intent(this, SettingsActivity.class);
                //myIntent.putExtra("key", value); //Optional parameters
                this.startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public void addAlarm(Activity activity, AlarmViewModel viewModel) {
        // TODO: only ask user if an alarm is activated, so when adding one but also when opening the app
        NotificationManager.manageNotificationPermission(activity);

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int timeFormat = TimeFormat.CLOCK_24H;
        if (!DateFormat.is24HourFormat(activity)) {
            timeFormat = TimeFormat.CLOCK_12H;
        }
        MaterialTimePicker materialTimePicker = new MaterialTimePicker.Builder()
                .setTimeFormat(timeFormat)
                .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                .setHour(hour)
                .setMinute(minute)
                .build();
        materialTimePicker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // name and days should be ask to the user not set in stone
                AlarmModel alarmModel = new AlarmModel("", materialTimePicker.getHour(), materialTimePicker.getMinute(), 0, true);
                alarmModel.setProfile(profileModel);

                viewModel.addItem(activity, alarmModel);
            }
        });
        materialTimePicker.show(getSupportFragmentManager(), "alarm_timepicker");
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapseLayout);
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitle(getApplicationName(this));
        //Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        //setSupportActionBar(myToolbar);

        Log.d(msg, "The onCreate() event");
        ActivityResultLauncher<Intent> directoryPickerLauncher = ProfileFactory.getDirectoryPickerLauncher(this);
        profileFactory = new ProfileFactory(this, directoryPickerLauncher);
        defaultProfile = new DefaultProfileTemplate();

        // TODO: move to external class to follow boot receiver
        AlarmDatabase db = AlarmDatabase.getDatabase(this);
        AlarmModelDao alarmModelDao = db.alarmModel();
        Executor executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
            BootReceiver.activateAlarms(alarmModelDao, this);

            if (!profileFactory.doesProfileAlreadyExist(defaultProfile))
                profileModel = profileFactory.getLastVersionOfProfile(defaultProfile);
            else
                profileModel = profileFactory.getDatabaseVersionOfProfile(defaultProfile);

            profileAreLoaded = true;
            if (menu != null)
                menu.findItem(R.id.action_alarm).setVisible(true);
        });

        if (BuildConfig.DEBUG) {
            ListView itemsListView = (ListView) findViewById(R.id.list_view);
            ArrayList<Item> itemsArrayList = new ArrayList<>(List.of(new Item("toto", false)));
            TestAlarmAdapter adapter = new TestAlarmAdapter(this, this, itemsArrayList, directoryPickerLauncher);
            itemsListView.setAdapter(adapter);
            itemsListView.setNestedScrollingEnabled(true);

            View curve = (View) findViewById(R.id.bezier);
            curve.setVisibility(View.VISIBLE);
        }

        viewModel = new ViewModelProvider(this).get(AlarmViewModel.class);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        AlarmAdapter recyclerViewAdapter = new AlarmAdapter(this, new ArrayList<AlarmModel>(), viewModel, directoryPickerLauncher);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        NotificationManager.createNotificationChannel(this);

        Activity activity = this;
        LifecycleOwner owner = this;
        viewModel.getAlarmList().observe(MainActivity.this, new Observer<List<AlarmModel>>() {
            @Override
            public void onChanged(@Nullable List<AlarmModel> alarmModels) {
                if (alarmModels != null) {
                    for (AlarmModel alarm : alarmModels) {
                        AlarmDatabase.getDatabase(activity.getApplication()).profileModel().getItemById(alarm.getProfileId()).observe(owner, new Observer<ProfileModel>() {
                            @Override
                            public void onChanged(@Nullable ProfileModel profileModel) {
                                if (profileModel != null)
                                    alarm.setProfile(profileModel);

                                recyclerViewAdapter.changeItems(alarmModels);
                            }
                        });
                    }
                }
            }
        });
    }

    /** Called when the activity is about to become visible. */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "The onResume() event");
    }

    /** Called when another activity is taking focus. */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** Called when the activity is no longer visible. */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    /** Called just before the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }
}
