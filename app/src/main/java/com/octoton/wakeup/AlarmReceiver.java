package com.octoton.wakeup;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.octoton.wakeup.AlarmManager.AlarmService;
import com.octoton.wakeup.DataModel.Synchronous.AlarmModelSynchronous;
import com.octoton.wakeup.Notification.PreviewNotification;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AlarmReceiver extends BroadcastReceiver {
   String msg = "Broadcast : ";

   public static String START_ALARM = "com.octoton.wakeup.AlarmBroadcast";
   public static String STOP_ALARM = "com.octoton.wakeup.StopAlarm";
   public static String SNOOZE_ALARM = "com.octoton.wakeup.SnoozeAlarm";
   public static String START_PREVIEW_ALARM = "com.octoton.wakeup.StartPreviewAlarm";
   public static String STOP_ALARM_FROM_PREVIEW = "com.octoton.wakeup.StopAlarmFromPreview";
   public static String GET_STEP = "com.octoton.wakeup.GetStep";
   public static String STEP_NUMBER = "step_number";

   public static String STEP_DAY = "step_day";
   public static String NOTIFICATION_ID = "notification-id";
   public static String NOTIFICATION = "notification";
   public static String CHANNEL_ID = "channel_id";

   @Override
   public void onReceive(Context context, Intent intent) {
      Log.d(msg, "start");
      Log.d(msg, intent.getAction());
      if ((SNOOZE_ALARM).equals(intent.getAction()) || (START_ALARM).equals(intent.getAction()) || (STOP_ALARM).equals(intent.getAction())) {
         // perform your scheduled task here (eg. send alarm notification)
         int id = intent.getIntExtra(NOTIFICATION_ID, 0);
         int step = intent.getIntExtra(STEP_NUMBER, 0);
         int day = intent.getIntExtra(STEP_DAY, 0);

         try {
            Intent intentService = new Intent(context, AlarmService.class);
            intentService.putExtra(AlarmService.ID, id);
            intentService.putExtra(AlarmService.ACTION, intent.getAction());
            intentService.putExtra(AlarmService.STEP, step);
            intentService.putExtra(AlarmService.DAY, day);
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intentService);
            //} else {
            //   context.startService(intentService );
            //}
         } catch (Exception e) {
            e.printStackTrace();
         }
      } else if ((STOP_ALARM_FROM_PREVIEW).equals(intent.getAction()) || (START_PREVIEW_ALARM).equals(intent.getAction())) {
         int id = intent.getIntExtra(NOTIFICATION_ID, 0);

         AlarmModelSynchronous alarmModel = new AlarmModelSynchronous(context);

         Executor executor = Executors.newSingleThreadExecutor();
         executor.execute(() -> {
            if ((STOP_ALARM_FROM_PREVIEW).equals(intent.getAction())) {
               alarmModel.ignoreAlarm(id);
            } else {
               alarmModel.setPreview(id);
            }
         });
      }
   }
}
