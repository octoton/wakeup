package com.octoton.wakeup.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ProfileModelDao {
    @Query("select * from ProfileModel where id = :id")
    LiveData<ProfileModel> getItemById(int id);

    @Query("select * from ProfileModel where id = :id")
    ProfileModel getItemByIdSynchronous(int id);

    @Query("select * from ProfileModel where name = :name")
    ProfileModel getItemByNameSynchronous(String name);

    @Query("select * from ProfileModel")
    List<ProfileModel> getAllProfileSynchronous();

    @Query("select COUNT() from ProfileModel where name = :name")
    int countItemByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addProfile(ProfileModel moduleModel);

    @Delete
    void deleteProfile(ProfileModel moduleModel);

    @Update
    void updateProfile(ProfileModel moduleModel);
}
