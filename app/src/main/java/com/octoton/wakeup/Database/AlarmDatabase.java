package com.octoton.wakeup.Database;

import android.content.Context;

import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.DeleteColumn;
import androidx.room.RenameTable;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {AlarmModel.class, ModuleModel.class, ProfileModel.class}, version = 2, autoMigrations = { @AutoMigration(from = 1, to = 2, spec = AlarmDatabase.AutoMigration.class) })
public abstract class AlarmDatabase extends RoomDatabase {

    @DeleteColumn(tableName = "AlarmModel", columnName = "isIgnored")
    @DeleteColumn(tableName = "AlarmModel", columnName = "previewDone")
    static class AutoMigration implements AutoMigrationSpec {   }

    private static AlarmDatabase INSTANCE;

    public static AlarmDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AlarmDatabase.class, "alarm_db")
                            .build();
        }
        return INSTANCE;
    }

    public abstract AlarmModelDao alarmModel();

    public abstract ProfileModelDao profileModel();

    public abstract ModuleModelDao moduleModel();

}
