package com.octoton.wakeup.Database;

import android.util.Log;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity
public class ProfileModel {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String name;
    @Ignore
    private List<ModuleModel> moduleList;

    public ProfileModel(String name) {
        this.name = name;
        this.moduleList = null;
    }

    public void setModuleList(List<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public List<ModuleModel> getModuleList() {
        return moduleList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;

        ProfileModel profileModel = (ProfileModel) object;
        if (!this.name.equals(profileModel.name)) return false;
        if (this.moduleList != null && profileModel.moduleList != null) {
            if (this.moduleList.size() != profileModel.moduleList.size()) return false;

            List<ModuleModel> work = new ArrayList<>(profileModel.getModuleList());
            for (ModuleModel module : this.moduleList) {
                if (!work.remove(module)) {
                    return false;
                }
            }
            return work.isEmpty();
        } else {
            return this.moduleList == null && profileModel.moduleList == null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;

        hash = 89 * hash + (this.name == null ? 0 :this.name.hashCode());
        hash = 89 * hash + (this.moduleList == null ? 0 : this.moduleList.hashCode());
        return hash;
    }
}
