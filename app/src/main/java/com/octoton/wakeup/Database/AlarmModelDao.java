package com.octoton.wakeup.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AlarmModelDao {

    @Query("select * from AlarmModel where isTesting = 1 limit 1")
    AlarmModel getTestAlarmItemSynchronous();

    @Query("select * from AlarmModel where isTesting = 0 order by hour ASC, minute ASC")
    LiveData<List<AlarmModel>> getAllAlarmItems();

    @Query("select * from AlarmModel")
    List<AlarmModel> getAllAlarmItemsSynchronous();

    @Query("select * from AlarmModel where id = :id")
    LiveData<AlarmModel> getItemById(int id);

    @Query("select * from AlarmModel where id = :id")
    AlarmModel getItemByIdSynchronous(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addAlarm(AlarmModel alarmModel);

    @Delete
    void deleteAlarm(AlarmModel alarmModel);

    @Update
    void updateAlarm(AlarmModel alarmModel);

}
