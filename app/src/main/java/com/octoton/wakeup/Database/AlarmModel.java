package com.octoton.wakeup.Database;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.octoton.wakeup.R;
import com.octoton.wakeup.WakeUp;

import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = ProfileModel.class,
                parentColumns = "id",
                childColumns = "profileId"
        )
})
public class AlarmModel {

    @PrimaryKey(autoGenerate = true)
    public int id;
    private String name;
    private boolean isActivated;
    private int hour;
    private int minute;
    //@ColumnInfo(defaultValue = "0") // because of migration from 1 to 2
    private int days;
    public boolean isTesting; // for quicker test
    @Ignore
    public static int testingSecondDelay = 8;
    @ColumnInfo(name = "profileId", index = true)
    private int profileId;
    @Ignore
    private ProfileModel profile;

    @ColumnInfo(defaultValue = "0")
    private int flags;

    private enum FLAG {
        None(0x00000000),
        AlarmIgnored(0x00000001),
        PreviewShown(0x00000010),
        PreviewDismissed(0x00000100),
        AlarmSnoozed(0x00001000);

        private final int value;
        FLAG(int value) {
            this.value = value;
        }

        public int get() { return this.value; }
    };
    private void addFlags(FLAG toAdd) {
        this.flags |= toAdd.get();
    }

    private void removeFlags(FLAG toRemove) {
        this.flags &= ~toRemove.get();
    }

    private boolean isFlagActivated(FLAG flag) {
        return (this.flags & flag.get()) > 0;
    }

    public AlarmModel(String name, int hour, int minute, int days, boolean isActivated) {
        this.name = name;
        this.hour = hour;
        this.minute = minute;
        this.isActivated = isActivated;
        this.days = days;

        this.profileId = 0;
        this.profile = null;

        this.isTesting = false;

        this.flags = FLAG.None.get();
    }

    public int getProfileId() { return profileId; }
    public void setProfileId(int profileId) { this.profileId = profileId; }
    public void setProfile(ProfileModel profile) {
        this.profile = profile;
        this.profileId = profile.id;
    }
    public ProfileModel getProfile() { return profile; }

    public String getName() {
        return name;
    }
    public int getHour() {
        return hour;
    }
    public int getMinute() {
        return minute;
    }
    public boolean getIsActivated() {
        return isActivated;
    }
    public int getDays() { return days; }
    public boolean isDayActivated(DAY day) {
        return (this.days & day.getFlag()) == day.getFlag();
    }
    public void setFlags(int flags) { this.flags = flags; }
    public int getFlags() { return this.flags; }

    public boolean hasItBeenIgnored() { return isFlagActivated(FLAG.AlarmIgnored); }

    public boolean hasPreviewBeenDismiss() { return isFlagActivated(FLAG.PreviewDismissed); }

    public boolean hasPreviewAlreadyBeenShown() { return isFlagActivated(FLAG.PreviewShown); }

    public boolean hasBeenSnoozed() { return isFlagActivated(FLAG.AlarmSnoozed); }

    public void resetFlags() { this.flags = FLAG.None.get(); }

    public void setAsIgnored() { if (isActivated) addFlags(FLAG.AlarmIgnored); }

    public void dismissPreview() { if (isActivated) addFlags(FLAG.PreviewDismissed); }

    public void showPreview() { if (isActivated) addFlags(FLAG.PreviewShown); }
    public void previewHasNotBeenShown() { removeFlags(FLAG.PreviewShown); }

    public void setHasSnoozed() { if (isActivated) addFlags(FLAG.AlarmSnoozed); }

    public DAY convertDay(int day) {
        DAY convertedDay = null;
        Set<AlarmModel.DAY> weekdays = EnumSet.allOf(AlarmModel.DAY.class);
        for (AlarmModel.DAY weekday : weekdays) {
            if (weekday.getWeekday() == day) {
                convertedDay = weekday;
            }
        }
        return convertedDay;
    }

    int countDay() {
        int numberActive = 0;
        Set<AlarmModel.DAY> weekdays = EnumSet.allOf(AlarmModel.DAY.class);
        for (AlarmModel.DAY weekday : weekdays) {
            if (isDayActivated(weekday)) {
                numberActive += 1;
            }
        }
        return numberActive;
    }

    public boolean isThereLooping() {
        return this.days != 0;
    }

    public boolean isDayActivated(int day) {
        return isDayActivated(convertDay(day));
    }

    public static int getDay(int day, int add) {
        return ((day+add) - 1) % 7 + 1; //+1 and -1 because day are between 1 and 7, not 0 and 6
    }
    public static int getNextDay(int day) {
        return getDay(day, 1);
    }

    public int getFirstActivatedDay(int day) {
        for (int i=0; i<7; i++) {
            int nextDay = getDay(day, i);

            if (isDayActivated(nextDay)) {
                return nextDay;
            }
        }
        return day;
    }

    public int inHowMuchWeekIsFirstActivatedDay(int day) {
        boolean isNextWeek = false;
        for (int i=0; i<7; i++) {
            int nextDay = getDay(day, i);

            if (nextDay == Calendar.getInstance().getFirstDayOfWeek()) isNextWeek = true;
            if (isDayActivated(nextDay)) {
                return (isNextWeek) ? 1 : 0;
            }
        }
        return 0;
    }

    public int inHowMuchDayIsNextActivatedDay(int day) {
        int diff = 0;
        for (int i=0; i<7; i++) {
            int nextDay = getNextDay(day+i);

            if (isDayActivated(nextDay)) {
                diff = i+1;
                break;
            }
        }
        return diff;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDay(DAY day, boolean activated) {
        if (activated) {
            this.days = (this.days | day.getFlag());
        } else {
            this.days = (this.days ^ day.getFlag());
        }
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setIsActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    Calendar setupCalendar(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, getHour());
        calendar.set(Calendar.MINUTE, getMinute());
        if (isTesting)
            calendar.add(Calendar.SECOND, testingSecondDelay);
        else
            calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    public Calendar getNextCalendar(Calendar startDay, Calendar today) {
        if (isThereLooping()) {
            if (today.after(startDay) || !isDayActivated(startDay.get(Calendar.DAY_OF_WEEK))) {
                int amount = inHowMuchDayIsNextActivatedDay(startDay.get(Calendar.DAY_OF_WEEK));
                startDay.add(Calendar.DAY_OF_WEEK, amount);
            }
        } else {
            if (today.compareTo(startDay) >= 0) {
                startDay.add(Calendar.DAY_OF_WEEK, 1);
            }
        }
        return startDay;
    }

    public Calendar getCalendar() {
        Calendar today = Calendar.getInstance();
        return getNextCalendar(setupCalendar((Calendar) today.clone()), today);
    }

    @Nullable
    public Calendar getNextCalendar() {
        if (countDay() > 1) {
            Calendar closestAlarm = getCalendar();
            Calendar today = (Calendar) closestAlarm.clone();
            today.add(Calendar.MINUTE, 1);
            return getNextCalendar(closestAlarm, today);
        } else {
            return null;
        }

        /*Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, getHour());
        calendar.set(Calendar.MINUTE, getMinute());
        calendar.set(Calendar.SECOND, 0);
        if (isTesting)
            calendar.add(Calendar.SECOND, testingSecondDelay);
        else
            calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        int amount = inHowMuchDayIsNextActivatedDay(calendar.get(Calendar.DAY_OF_WEEK));
        if (amount >= 1) {
            calendar.add(Calendar.DAY_OF_WEEK, amount);
        } else {
            return null;
        }

        // TODO: break set nextCalendar after stopping current alarm
        if (calendar.equals(getCalendar())) {
            calendar.add(Calendar.DAY_OF_WEEK, inHowMuchDayIsNextActivatedDay(calendar.get(Calendar.DAY_OF_WEEK)));
        }

        return calendar;*/
    }

    private String getDiffDateString(Context context, Date startDate, Date endDate) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long different = endDate.getTime() - startDate.getTime();
        long elapsedDays = different / daysInMilli;

        different = different % daysInMilli;
        long elapsedHours = different / hoursInMilli;

        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;

        if (elapsedDays > 0)
            return context.getResources().getString(R.string.set_alarm, elapsedDays, elapsedHours, elapsedMinutes);
        else if (elapsedHours > 0)
            return context.getResources().getString(R.string.set_alarm_no_day, elapsedHours, elapsedMinutes);
        else if (elapsedMinutes > 0)
            return context.getResources().getString(R.string.set_alarm_no_hour, elapsedMinutes);
        else
            return context.getResources().getString(R.string.set_alarm_less_minute);
    }

    public void showToast(Context context, boolean force) {
        showToast(context, null, force);
    }

    public void showToast(Context context, AlarmModel oldAlarm) {
        showToast(context, oldAlarm, false);
    }

    public void showToast(Context context, AlarmModel oldAlarm, boolean force) {
        if (force || isActivated) {
            if (oldAlarm == null || !oldAlarm.isActivated || getCalendar().compareTo(oldAlarm.getCalendar()) != 0)
                WakeUp.showAlarmToast(context, getDiffDateString(context, Calendar.getInstance().getTime(), getCalendar().getTime()), Toast.LENGTH_LONG);
        } else if (oldAlarm == null || oldAlarm.isActivated) {
            WakeUp.showAlarmToast(context, context.getResources().getString(R.string.remove_alarm), Toast.LENGTH_SHORT);
        }
    }

    public enum DAY {
        MONDAY(Calendar.MONDAY, 0b1000000),
        TUESDAY(Calendar.TUESDAY, 0b0100000),
        WEDNESDAY(Calendar.WEDNESDAY, 0b0010000),
        THURSDAY(Calendar.THURSDAY, 0b0001000),
        FRIDAY(Calendar.FRIDAY, 0b0000100),
        SATURDAY(Calendar.SATURDAY, 0b0000010),
        SUNDAY(Calendar.SUNDAY, 0b0000001);

        public static final int WORKDAY = 0b1111100;

        private  final int weekday;
        private final int flag;
        DAY(int weekday, int flag) { this.weekday = weekday; this.flag = flag; }
        public int getFlag() { return flag; }
        public int getWeekday() { return weekday; }
    }
}
