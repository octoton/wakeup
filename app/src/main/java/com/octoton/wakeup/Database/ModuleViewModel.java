package com.octoton.wakeup.Database;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ModuleViewModel extends AndroidViewModel {

    private AlarmDatabase alarmDatabase;
    private final Executor executor;

    public ModuleViewModel(@NonNull Application application) {
        super(application);

        this.alarmDatabase = AlarmDatabase.getDatabase(this.getApplication());
        this.executor = Executors.newSingleThreadExecutor();
    }

    public void updateItem(Context context, ModuleModel moduleModel) {
        executor.execute(() -> alarmDatabase.moduleModel().updateModule(moduleModel));
    }

    private void addItem(ModuleModel moduleModel) {
        int id = (int)alarmDatabase.moduleModel().addModule(moduleModel);
    }

    public void deleteItem(ModuleModel moduleModel) {
        executor.execute(() -> alarmDatabase.moduleModel().deleteModule(moduleModel));
    }
}
