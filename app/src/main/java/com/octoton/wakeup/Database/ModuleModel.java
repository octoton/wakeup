package com.octoton.wakeup.Database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.ProvidedTypeConverter;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.Utils.BezierCurve;

import java.util.ArrayList;
import java.util.List;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = ProfileModel.class,
                parentColumns = "id",
                childColumns = "profileId"
        )
})
public class ModuleModel {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "profileId", index = true)
    private int profileId;

    @ColumnInfo(name = "moduleTag")
    @TypeConverters(ModuleTagConverter.class)
    ModuleManager.MODULE_TAG moduleTag;
    @Ignore
    private String sharedPreferenceName;

    private final String sharedPreferenceNameCommonPart;

    @Ignore
    private Module.Data data;

    private final int stepNumber;
    private final int duration;
    @Embedded
    private final BezierCurve activationCurve;

    public ModuleModel(ModuleManager.MODULE_TAG moduleTag, String sharedPreferenceNameCommonPart,
                       int stepNumber, int duration, BezierCurve activationCurve) {
        this(moduleTag, sharedPreferenceNameCommonPart, stepNumber, duration, activationCurve, null);
    }

    public ModuleModel(ModuleManager.MODULE_TAG moduleTag, String sharedPreferenceNameCommonPart,
                       int stepNumber, int duration, BezierCurve activationCurve,
                       Module.Data data) {
        this.moduleTag = moduleTag;
        this.sharedPreferenceNameCommonPart = sharedPreferenceNameCommonPart; //.split("\\.")[0];
        this.stepNumber = stepNumber;
        this.duration = duration;
        this.activationCurve = activationCurve;
        this.data = data;

        this.profileId = 0;
    }

    public Module.Data getData() { return data; }
    public void setData(Module.Data data) { this.data = data; }
    public ModuleManager.MODULE_TAG getModuleTag() { return moduleTag; }
    public String getSharedPreferenceNameCommonPart() { return sharedPreferenceNameCommonPart; }
    public String getSharedPreferenceName() { return this.preparedSharedPreferenceName(this.profileId); }
    public int getStepNumber() { return stepNumber; }
    public int getDuration() { return duration; }
    public BezierCurve getActivationCurve() { return activationCurve; }
    public int getProfileId() { return profileId; }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    private String preparedSharedPreferenceName(int profileId) {
        return this.sharedPreferenceName = this.sharedPreferenceNameCommonPart + "." + profileId + "." + this.stepNumber;
    }
    public void preparedSharedPreferenceName() {
        this.sharedPreferenceName = this.preparedSharedPreferenceName(this.profileId); //TODO: could also concatenate when needed, not really sure which is best
    }

    @NonNull
    @Override
    public String toString() {
        String value = "";

        value += "MODULE TAG: "+this.moduleTag;
        value += ", step number: "+this.stepNumber;
        value += ", duration: "+this.duration;
        value += ", curve: "+this.activationCurve.toString();
        value += ", preference name: "+this.sharedPreferenceNameCommonPart;

        return value;
    }

    public boolean wasEquals(Object object) {
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;

        ModuleModel moduleModel = (ModuleModel) object;

        if (this.moduleTag != moduleModel.moduleTag) return false;
        if (this.stepNumber != moduleModel.stepNumber) return false;

        return true;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;

        ModuleModel moduleModel = (ModuleModel) object;

        if (this.moduleTag != moduleModel.moduleTag) return false;
        if (this.stepNumber != moduleModel.stepNumber) return false;
        if (this.duration != moduleModel.duration) return false;
        if (!this.activationCurve.equals(moduleModel.activationCurve)) return false;
        if (!this.sharedPreferenceNameCommonPart.equals(moduleModel.sharedPreferenceNameCommonPart)) return false;
        if (!this.data.equals(moduleModel.data)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;

        hash = 89 * hash + (this.moduleTag == null ? 0 :this.moduleTag.hashCode());
        hash = 89 * hash + this.stepNumber;
        hash = 89 * hash + this.duration;
        hash = 89 * hash + (this.activationCurve == null ? 0 :this.activationCurve.hashCode());
        hash = 89 * hash + (this.sharedPreferenceNameCommonPart == null ? 0 :this.sharedPreferenceNameCommonPart.hashCode());

        return hash;
    }

    static class ModuleTagConverter {

        @TypeConverter
        public int fromModule(ModuleManager.MODULE_TAG moduleTag) { return moduleTag.getTag(); }

        @TypeConverter
        public ModuleManager.MODULE_TAG toModule(int tag) {
            for (ModuleManager.MODULE_TAG moduleTag : ModuleManager.MODULE_TAG.values()) {
                if (moduleTag.getTag() == tag) { return moduleTag; }
            }
            return null;
        }
    }
}
