package com.octoton.wakeup.Database;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.NotificationManager;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ProfileViewModel extends AndroidViewModel {

    private AlarmDatabase alarmDatabase;
    private final Executor executor;

    public ProfileViewModel(@NonNull Application application) {
        super(application);

        this.alarmDatabase = AlarmDatabase.getDatabase(this.getApplication());
        this.executor = Executors.newSingleThreadExecutor();
    }

    private int addItemInternal(Context context, ProfileModel profileModel) {
        int id = (int)alarmDatabase.profileModel().addProfile(profileModel);

        for (ModuleModel module : profileModel.getModuleList()) {
            module.setProfileId(id);
            //module.preparedSharedPreferenceName();
            alarmDatabase.moduleModel().addModule(module);

            ModuleManager.write(context, module.getSharedPreferenceName(), module.getModuleTag(), module.getData());
        }
        return id;
    }

    public void addItem(Context context, ProfileModel profileModel) {
        executor.execute(() -> addItemInternal(context, profileModel));
    }

    public int addItemSynchronously(Context context, ProfileModel profileModel) {
        return addItemInternal(context, profileModel);
    }

    public void updateItem(Context context, ProfileModel profileModel) {
        executor.execute(() -> {
            List<ModuleModel> moduleModels = alarmDatabase.moduleModel().getAllModuleItemsOfProfileSynchronous(profileModel.id);
            for (ModuleModel moduleModel : profileModel.getModuleList()) {
                boolean found = false;
                Iterator<ModuleModel> i = moduleModels.iterator();
                while (i.hasNext()) {
                    ModuleModel sys_module = i.next();
                    if (moduleModel.wasEquals(sys_module)) {
                        moduleModel.id = sys_module.id;
                        alarmDatabase.moduleModel().updateModule(moduleModel);
                        found = true;

                        ModuleManager.remove(context, moduleModel.getSharedPreferenceName());
                        ModuleManager.write(context, moduleModel.getSharedPreferenceName(), moduleModel.getModuleTag(), moduleModel.getData());

                        i.remove();
                    }
                }
                if (!found) {
                    alarmDatabase.moduleModel().addModule(moduleModel);
                    ModuleManager.write(context, moduleModel.getSharedPreferenceName(), moduleModel.getModuleTag(), moduleModel.getData());
                }
            }
            for (ModuleModel moduleModel: moduleModels) {
                alarmDatabase.moduleModel().deleteModule(moduleModel);
                ModuleManager.remove(context, moduleModel.getSharedPreferenceName());
            }
            alarmDatabase.profileModel().updateProfile(profileModel);
        });
    }

    public void deleteItem(Context context, ProfileModel profileModel) {
        executor.execute(() -> {
            List<ModuleModel> moduleModels = alarmDatabase.moduleModel().getAllModuleItemsOfProfileSynchronous(profileModel.id);
            for (ModuleModel module : moduleModels) {
                ModuleManager.remove(context, module.getSharedPreferenceName());
            }
            alarmDatabase.moduleModel().deleteAllModulesOfProfile(profileModel.id);
            alarmDatabase.profileModel().deleteProfile(profileModel);
        });
    }
}
