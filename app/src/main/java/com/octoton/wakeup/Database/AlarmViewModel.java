package com.octoton.wakeup.Database;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.octoton.wakeup.Notification.AlarmNotification;
import com.octoton.wakeup.Notification.PreviewNotification;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AlarmViewModel extends AndroidViewModel {

    private final LiveData<List<AlarmModel>> alarmList;
    private AlarmDatabase alarmDatabase;
    private final Executor executor;

    public AlarmViewModel(Application application) { //}, Executor executor) {
        super(application);

        this.alarmDatabase = AlarmDatabase.getDatabase(this.getApplication());
        this.alarmList = alarmDatabase.alarmModel().getAllAlarmItems();
        this.executor = Executors.newSingleThreadExecutor(); //executor;
    }

    public LiveData<List<AlarmModel>> getAlarmList() {
        return alarmList;
    }

    public void addItem(Context context, AlarmModel alarmModel) {
        executor.execute(() -> {
            int id = (int)alarmDatabase.alarmModel().addAlarm(alarmModel);

            alarmModel.id = id;
            if (alarmModel.getIsActivated()) {
                alarmModel.showPreview(); // TODO: only if preview is really shown
                AlarmNotification.setAlarm(context, id, alarmModel.getCalendar(), alarmModel.hasItBeenIgnored(), alarmModel.hasPreviewAlreadyBeenShown());
            } else {
                AlarmNotification.removeAlarm(context, id, alarmModel.getCalendar(), alarmModel.getNextCalendar());
            }

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    alarmModel.showToast(context, false);
                }
            });
        });
    }

    public void updateItem(Context context, AlarmModel alarmModel) {
        executor.execute(() -> {
            AlarmModel oldAlarm = this.alarmDatabase.alarmModel().getItemByIdSynchronous(alarmModel.id);
            if (alarmModel.getIsActivated()) {
                if (oldAlarm == null || !oldAlarm.getIsActivated() || alarmModel.getCalendar().compareTo(oldAlarm.getCalendar()) != 0)
                    alarmModel.resetFlags();

                AlarmNotification.updateAlarm(context, alarmModel, oldAlarm); // TODO: this path cannot call get alarm through id, updateAlarm should be always done outside
            } else {
                alarmModel.resetFlags(); //TODO: found out why this doesn't seems to be working (activate alarm, wait for preview, desactivate alarm: flag are not reset)
                AlarmNotification.removeAlarm(context, alarmModel.id, oldAlarm.getCalendar(), oldAlarm.getNextCalendar());
                PreviewNotification.removeNotification(context, alarmModel.id);
                //TODO: remove snooze alarm and notification
            }
            this.alarmDatabase.alarmModel().updateAlarm(alarmModel);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    alarmModel.showToast(context, oldAlarm);
                }
            });
        });
    }

    public void addItemForTesting(Context context, AlarmModel alarmModel, Calendar cal) {
        executor.execute(() -> {
            int id = (int)alarmDatabase.alarmModel().addAlarm(alarmModel);

            AlarmNotification.setAlarm(context, id, cal, false, false);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    alarmModel.showToast(context, true);
                }
            });
        });
    }

    public void updateItemForTesting(Context context, AlarmModel alarmModel, Calendar cal) {
        executor.execute(() -> {
            alarmDatabase.alarmModel().updateAlarm(alarmModel);

            AlarmNotification.setAlarm(context, alarmModel.id, cal, false, false);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    alarmModel.showToast(context, true);
                }
            });
        });
    }

    public void deleteItem(Context context, AlarmModel alarmModel) {
        AlarmNotification.removeAlarm(context, alarmModel.id, alarmModel.getCalendar(), alarmModel.getNextCalendar());

        executor.execute(() -> {
            AlarmNotification.removeAlarm(context, alarmModel.id, alarmModel.getCalendar(), alarmModel.getNextCalendar());
            PreviewNotification.removeNotification(context, alarmModel.id);

            alarmDatabase.alarmModel().deleteAlarm(alarmModel);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    alarmModel.showToast(context, false);
                }
            });
        });
    }

    public LiveData<AlarmModel> getItemById(int id) {
        return alarmDatabase.alarmModel().getItemById(id);
    }
}
