package com.octoton.wakeup.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.octoton.wakeup.AlarmManager.Module.ModuleManager;

import java.util.List;

@Dao
public interface ModuleModelDao {
    @Query("select * from ModuleModel where profileId = :id")
    List<ModuleModel> getAllModuleItemsOfProfileSynchronous(int id);

    @Query("select * from ModuleModel where profileId = :id and moduleTag >= " + ModuleManager.MODULE_TAG.GRAPHICAL_OFFSET + " and stepNumber = :stepNumber")
    List<ModuleModel> getAllGraphicalModuleItemsOfProfileSynchronous(int id, int stepNumber);

    @Query("select * from ModuleModel where profileId = :id and moduleTag < " + ModuleManager.MODULE_TAG.GRAPHICAL_OFFSET + " and stepNumber = :stepNumber")
    List<ModuleModel> getAllServiceModuleItemsOfProfileSynchronous(int id, int stepNumber);

    @TypeConverters(ModuleModel.ModuleTagConverter.class)
    @Query("select moduleTag from ModuleModel where profileId = :id and stepNumber = :stepNumber")
    List<ModuleManager.MODULE_TAG> getAllModuleItemsOfProfileSynchronous(int id, int stepNumber);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addModule(ModuleModel moduleModel);

    @Delete
    void deleteModule(ModuleModel moduleModel);

    @Query("delete from ModuleModel where profileId = :id")
    void deleteAllModulesOfProfile(int id);

    @Query("delete from ModuleModel where profileId = :id and stepNumber = :stepNumber")
    void deleteAllModulesOfProfileForSpecificStepNumber(int id, int stepNumber);

    @Update
    void updateModule(ModuleModel moduleModel);

    @Query("select MAX(duration) from ModuleModel where profileId = :id and stepNumber = :stepNumber")
    int getMaxDurationInProfileModuleItemsOfProfileSynchronous(int id, int stepNumber);

    @Query("select MAX(stepNumber) from ModuleModel where profileId = :id")
    int getMaxStepNumberInProfileModuleItemsOfProfileSynchronous(int id);
}
