package com.octoton.wakeup.DataModel.Synchronous;

import android.content.Context;

import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.DataModel.Interface.ProfileModelInterface;
import com.octoton.wakeup.Database.AlarmDatabase;

import java.util.List;

public class ProfileModelSynchronous implements ProfileModelInterface {

    AlarmDatabase database;

    public ProfileModelSynchronous(Context context) {
        this.database = AlarmDatabase.getDatabase(context);
    }

    @Override
    public int getMaxStepNumberInModuleItemsOfProfile(int id) {
        return this.database.moduleModel().getMaxStepNumberInProfileModuleItemsOfProfileSynchronous(id);
    }

    @Override
    public int getMaxDurationInModuleItemsOfProfileForStep(int id, int stepNumber) {
        return this.database.moduleModel().getMaxDurationInProfileModuleItemsOfProfileSynchronous(id, stepNumber);
    }

    @Override
    public List<ModuleManager.MODULE_TAG> getAllModuleItemsOfProfile(int id, int stepNumber) {
        return this.database.moduleModel().getAllModuleItemsOfProfileSynchronous(id, stepNumber);
    }
}
