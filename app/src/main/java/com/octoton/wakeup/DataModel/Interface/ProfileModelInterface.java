package com.octoton.wakeup.DataModel.Interface;

import com.octoton.wakeup.AlarmManager.Module.ModuleManager;

import java.util.List;

public interface ProfileModelInterface {
    int getMaxStepNumberInModuleItemsOfProfile(int id);
    int getMaxDurationInModuleItemsOfProfileForStep(int id, int stepNumber);
    List<ModuleManager.MODULE_TAG> getAllModuleItemsOfProfile(int id, int stepNumber);
}
