package com.octoton.wakeup.DataModel.Synchronous;

import android.content.Context;

import com.octoton.wakeup.DataModel.Interface.AlarmModelInterface;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Notification.AlarmNotification;
import com.octoton.wakeup.Notification.PreviewNotification;
import com.octoton.wakeup.Notification.SnoozeNotification;

public class AlarmModelSynchronous implements AlarmModelInterface {

    Context context;
    AlarmDatabase database;

    public AlarmModelSynchronous(Context context) {
        this.context = context;
        this.database = AlarmDatabase.getDatabase(context);
    }

    public int getProfileIdOfAlarm(int id) {
        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id);

        return alarmModel.getProfileId();
    }

    public void setNextAlarmIfPossibleFor(int id) {
        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id); // TODO: if this return null, it will crash the current alarm which is worse than not planning the next one (this should be a notification to the user)

        alarmModel.resetFlags();
        if (!alarmModel.isThereLooping()) {
            alarmModel.setIsActivated(false);
        }
        this.database.alarmModel().updateAlarm(alarmModel);

        if (alarmModel.getIsActivated())
            AlarmNotification.setAlarmOrNextIfIgnored(context, alarmModel);
    }

    public void ignoreAlarm(int id) {
        PreviewNotification.removeNotification(context, id);

        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id);

        if (alarmModel.isThereLooping()) {
            alarmModel.setAsIgnored();
        } else {
            alarmModel.setIsActivated(false);
        }
        alarmModel.previewHasNotBeenShown();

        this.database.alarmModel().updateAlarm(alarmModel);
        setNotification(alarmModel);
    }

    public void setPreview(int id) {
        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id);

        alarmModel.showPreview();
        this.database.alarmModel().updateAlarm(alarmModel);
        setNotification(alarmModel);

        PreviewNotification.setNotification(this.context, id, alarmModel.getCalendar());
    }

    private void setNotification(AlarmModel alarmModel) {
        if (alarmModel.getIsActivated()) {
            AlarmNotification.updateAlarm(this.context, alarmModel, null);
        } else {
            AlarmNotification.removeAlarm(this.context, alarmModel.id, alarmModel.getCalendar(), alarmModel.getNextCalendar());
        }
    }

    public void snooze(int id, int stepNumber, int day, int durationInMilli, int notificationId) {
        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id);

        alarmModel.setHasSnoozed();
        this.database.alarmModel().updateAlarm(alarmModel);

        SnoozeNotification.set(this.context, id, stepNumber, day, durationInMilli, notificationId);
    }

    public void stop(int id, int stepNumber, int day, int notificationId) {
        AlarmModel alarmModel = this.database.alarmModel().getItemByIdSynchronous(id);

        alarmModel.resetFlags();
        this.database.alarmModel().updateAlarm(alarmModel);

        SnoozeNotification.remove(this.context, id, stepNumber, day, notificationId);
        AlarmNotification.removeAlarm(this.context, id, stepNumber, day);
        AlarmNotification.removeNotification(this.context, notificationId);
    }
}
