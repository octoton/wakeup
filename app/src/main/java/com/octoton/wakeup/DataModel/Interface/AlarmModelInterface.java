package com.octoton.wakeup.DataModel.Interface;

public interface AlarmModelInterface {
    int getProfileIdOfAlarm(int id);
    void setNextAlarmIfPossibleFor(int id);
}
