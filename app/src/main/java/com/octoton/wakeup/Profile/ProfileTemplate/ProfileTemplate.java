package com.octoton.wakeup.Profile.ProfileTemplate;

import android.app.Activity;

import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.ProfileModel;

public interface ProfileTemplate {
    ProfileModel create(Activity activity);
    ProfileModel get(AlarmDatabase database);
    int count(AlarmDatabase database);
    boolean needDirectoryAccess();
    String getName();
}
