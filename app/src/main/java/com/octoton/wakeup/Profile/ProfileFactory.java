package com.octoton.wakeup.Profile;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.Database.ProfileViewModel;
import com.octoton.wakeup.Profile.ProfileTemplate.DefaultProfileTemplate;
import com.octoton.wakeup.Profile.ProfileTemplate.ProfileTemplate;
import com.octoton.wakeup.Profile.ProfileTemplate.TestingProfileTemplate;
import com.octoton.wakeup.Profile.ProfileTemplate.WakingProfileTemplate;

import java.util.ArrayList;
import java.util.List;

public class ProfileFactory {

    private final Activity activity;
    private final AlarmDatabase database;
    private final ProfileViewModel viewModel;
    private ActivityResultLauncher<Intent> directoryPickerLauncher;

    public ProfileFactory(AppCompatActivity activity, ActivityResultLauncher<Intent> directoryPickerLauncher) {
        this.activity = activity;
        this.database = AlarmDatabase.getDatabase(activity.getApplication());
        this.viewModel = new ViewModelProvider(activity).get(ProfileViewModel.class);

        this.directoryPickerLauncher = directoryPickerLauncher;
    }

    public static ActivityResultLauncher<Intent> getDirectoryPickerLauncher(AppCompatActivity activity) {
        return  activity.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == RESULT_OK) {
                            Intent data = result.getData();
                            if (data != null && data.getData() != null) {
                                Uri treeUri = data.getData();
                                activity.grantUriPermission(activity.getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                activity.getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                        }
                    }
                });
    }

    public boolean doesProfileNeedToBeUpdated(ProfileTemplate profileTemplate) {
        ProfileModel newProfileModel = profileTemplate.create(activity);
        ProfileModel currentProfileModel = profileTemplate.get(database);
        if (currentProfileModel != null) {
            List<ModuleModel> list = database.moduleModel().getAllModuleItemsOfProfileSynchronous(currentProfileModel.id);
            for (ModuleModel module : list) {
                module.id = currentProfileModel.id;
                module.setData(ModuleManager.MODULE_TAG.getData(module.getModuleTag(), activity, module));
            }
            currentProfileModel.setModuleList(list);
        }

        return (!newProfileModel.equals(currentProfileModel));
    }

    public boolean doesProfileAlreadyExist(ProfileTemplate profileTemplate) {
        return profileTemplate.count(database) != 0;
    }

    public ProfileModel getDatabaseVersionOfProfile(ProfileTemplate profileTemplate) {
        return profileTemplate.get(database);
    }

    public ProfileModel getLastVersionOfProfile(ProfileTemplate profileTemplate) {
        ProfileModel newProfileModel = profileTemplate.create(activity);
        ProfileModel currentProfileModel = profileTemplate.get(database);
        if (currentProfileModel != null) {
            List<ModuleModel> list = database.moduleModel().getAllModuleItemsOfProfileSynchronous(currentProfileModel.id);
            for (ModuleModel module : list) {
                module.id = currentProfileModel.id;
                Module.Data data = ModuleManager.MODULE_TAG.getData(module.getModuleTag(), activity, module);
                module.setData(data);
            }
            currentProfileModel.setModuleList(list);
        }

        if (!newProfileModel.equals(currentProfileModel)) {
            if (currentProfileModel == null) {
                newProfileModel.id = viewModel.addItemSynchronously(activity, newProfileModel);
            } else {
                newProfileModel.id = currentProfileModel.id;
                for (ModuleModel moduleModel : newProfileModel.getModuleList()) {
                    moduleModel.setProfileId(newProfileModel.id);
                }
                viewModel.updateItem(activity, newProfileModel);
            }

            for (ModuleModel module: newProfileModel.getModuleList()) {
                ModuleManager.MODULE_TAG.getAccessRight(module, activity);
            }

            if (profileTemplate.needDirectoryAccess()) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                directoryPickerLauncher.launch(intent);
            }

            return newProfileModel;
        }

        return currentProfileModel;
    }

    public static ProfileTemplate getProfileTemplate(ProfileModel profileModel) {
        List<ProfileTemplate> templates = getAllProfileTemplate();

        for (ProfileTemplate profileTemplate: templates) {
            if (profileTemplate.getName().equals(profileModel.getName())) {
                return profileTemplate;
            }
        }

        return null;
    }

    public static List<ProfileTemplate> getAllProfileTemplate() {
        List<ProfileTemplate> templates = new ArrayList<>();
        templates.add(new DefaultProfileTemplate());
        templates.add(new WakingProfileTemplate());

        return templates;
    }
}
