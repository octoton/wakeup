package com.octoton.wakeup.Profile.ProfileTemplate;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Color;
import android.net.Uri;

import com.octoton.wakeup.AlarmManager.ClockFragment;
import com.octoton.wakeup.AlarmManager.ColorGradientFragment;
import com.octoton.wakeup.AlarmManager.FlashService;
import com.octoton.wakeup.AlarmManager.LightFragment;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.RepeatDismissFragment;
import com.octoton.wakeup.AlarmManager.SoundService;
import com.octoton.wakeup.AlarmManager.Utils.BezierCurve;
import com.octoton.wakeup.AlarmManager.Utils.Units;
import com.octoton.wakeup.AlarmManager.VibratorService;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.R;

import java.util.ArrayList;
import java.util.List;

public class TestingProfileTemplate implements ProfileTemplate {
    private final String name = "testing";

    public TestingProfileTemplate() {}

    @Override
    public ProfileModel create(Activity activity) {
        ProfileModel profileModel;

        profileModel = new ProfileModel(name);

        List<ModuleModel> moduleModels = new ArrayList<>();

        // TODO: add preference view
        String pathFolderSong = "content://com.android.externalstorage.documents/tree/primary%3AMusic%2FBrigitte%2FEt%20Vous%2C%20Tu%20M'Aimes%20_";
        //String pathFolderSong = "content://com.android.externalstorage.documents/tree/primary%3AMusic%2FDisney%2FEncanto";

        BezierCurve logCurve = new BezierCurve(0.01f, 0.01f, -1.0f, -0.0f);
        BezierCurve linearCurve = new BezierCurve(0.1f, 0.1f, -0.1f, -0.1f);
        int duration_0 = 6 * Units.s;
        int duration_1 = 6 * Units.s;
        int duration_2 = 20 * Units.s;
        int duration_3 = 6 * Units.s;

        // TODO: order seems important, not a good limitation
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.FLASH_SERVICE, FlashService.FlashData.SHARED_PREFERENCE, 0, duration_0, linearCurve, new FlashService.FlashData()));

        String rawSong = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(activity.getPackageName())
                .appendPath(String.valueOf(R.raw.bird_song))
                .build().toString();
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 0, duration_0, linearCurve,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(0xff0a6c58), Color.valueOf(0xff0a6c58))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 0, duration_0, linearCurve,
                new ClockFragment.ClockData(Color.valueOf(0xff0a6c58), Color.valueOf(0xff0a6c58), Color.valueOf(0xff292444), Color.valueOf(0xff292444), true, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 0, duration_0, linearCurve,
                //new RepeatDismissFragment.RepeatDismissData(context, true, false, true)));
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(0x19efefef), Color.valueOf(0x19efefef)},
                        new Color[]{Color.valueOf(0xff292444), Color.valueOf(0xff292444)},
                        false,
                        new Color[]{Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE)},
                        new Color[]{Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE)}
                )));

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 1, duration_1, linearCurve,
                new SoundService.SoundData(Uri.parse(pathFolderSong), SoundService.MAX_VOLUME, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 1, duration_1, linearCurve,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(0xff111e1f), Color.valueOf(0xff111e1f))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 1, duration_1, linearCurve,
                new ClockFragment.ClockData(Color.valueOf(0xff111e1f), Color.valueOf(0xff111e1f), Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00), true, true, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 1, duration_1, linearCurve,
                //new RepeatDismissFragment.RepeatDismissData(context, true, false, true)));
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        true,
                        new Color[]{Color.valueOf(0x19efefef), Color.valueOf(0x19efefef)},
                        new Color[]{Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00)},
                        true,
                        new Color[]{Color.valueOf(0xff111e1f), Color.valueOf(0xff111e1f)},
                        new Color[]{Color.valueOf(0xff111e1f), Color.valueOf(0xff111e1f)}
                )));

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 2, duration_2, linearCurve,
                new SoundService.SoundData(Uri.parse(pathFolderSong), SoundService.MAX_VOLUME, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 2, duration_2, logCurve,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(0xff111e1f), Color.valueOf(Color.WHITE))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 2, duration_2, logCurve,
                new ClockFragment.ClockData(Color.valueOf(0xff111e1f), Color.valueOf(Color.WHITE), Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00), true, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 2, duration_2, logCurve,
                //new RepeatDismissFragment.RepeatDismissData(context, true, false, true)));
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(0x19efefef), Color.valueOf(0x69d4d4d4)},
                        new Color[]{Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00)},
                        true,
                        new Color[]{Color.valueOf(0xff111e1f), Color.valueOf(Color.WHITE)},
                        new Color[]{Color.valueOf(0xff111e1f), Color.valueOf(Color.WHITE)}
                )));

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.VIBRATOR_SERVICE, VibratorService.VibratorData.SHARED_PREFERENCE, 3, duration_3, linearCurve, new VibratorService.VibratorData()));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 3, duration_3, linearCurve,
                new ClockFragment.ClockData(activity, Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE), false, false, true)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 3, duration_3, linearCurve,
                new RepeatDismissFragment.RepeatDismissData(activity, false, true, false)));

        profileModel.setModuleList(moduleModels);

        return profileModel;
    }

    @Override
    public ProfileModel get(AlarmDatabase database) {
        return database.profileModel().getItemByNameSynchronous(name);
    }

    @Override
    public int count(AlarmDatabase database) {
        return database.profileModel().countItemByName(name);
    }

    @Override
    public String getName() { return name; }

    @Override
    public boolean needDirectoryAccess() {
        return true;
    }
}
