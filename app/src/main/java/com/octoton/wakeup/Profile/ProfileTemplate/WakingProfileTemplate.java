package com.octoton.wakeup.Profile.ProfileTemplate;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Color;
import android.net.Uri;

import androidx.core.content.ContextCompat;

import com.octoton.wakeup.AlarmManager.ClockFragment;
import com.octoton.wakeup.AlarmManager.ColorGradientFragment;
import com.octoton.wakeup.AlarmManager.FlashService;
import com.octoton.wakeup.AlarmManager.LightFragment;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.RepeatDismissFragment;
import com.octoton.wakeup.AlarmManager.SoundService;
import com.octoton.wakeup.AlarmManager.Utils.BezierCurve;
import com.octoton.wakeup.AlarmManager.Utils.Units;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.R;

import java.util.ArrayList;
import java.util.List;

public class WakingProfileTemplate implements ProfileTemplate {
    private final String name = "waking up";

    @Override
    public ProfileModel create(Activity activity) {
        ProfileModel profileModel;

        profileModel = new ProfileModel(name);

        List<ModuleModel> moduleModels = new ArrayList<>();

        // TODO: add preference view
        BezierCurve bezierCurve = new BezierCurve(0.6f, 0.06f, -0.6f, -0.4f);
        BezierCurve bezierCurveLinear = new BezierCurve(0.6f, 0.4f, -0.5f, -0.5f);
        BezierCurve bezierCurve_1 = new BezierCurve(0.6f, 0.4f, -0.8f, -0.05f);
        int duration_0 = 5 * Units.min;
        int duration_1 = 5 * Units.min;
        int duration_2 = 5 * Units.min;

        // TODO: when preference, link directoryPickerLauncher result with pathFolderSong
        //String pathFolderSong = "content://com.android.externalstorage.documents/tree/primary%3AMusic%2FBrigitte%2FEt%20Vous%2C%20Tu%20M'Aimes%20_";
        String pathFolderSong = "content://com.android.externalstorage.documents/tree/primary%3AMusic%2FDisney%2FEncanto";

        // TODO: order seems important, not a good limitation
        String rawSong = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(activity.getPackageName())
                .appendPath(String.valueOf(R.raw.bluejaysspringwoods))
                .build().toString();
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new SoundService.SoundData(Uri.parse(rawSong), 0, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.LIGHT_SERVICE, LightFragment.LightData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new LightFragment.LightData(0, LightFragment.MAX_BRIGHTNESS)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(Color.BLACK), Color.valueOf(0xffff7f00))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new ClockFragment.ClockData(Color.valueOf(Color.BLACK), Color.valueOf(0xffBE4B00), Color.valueOf(Color.BLACK), Color.valueOf(0xffBE4B00), false, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(0xffBE4B00)},
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(0xffBE4B00)},
                        false,
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(0xffff7f00)},
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(0xffff7f00)}
                )));

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 1, duration_1, bezierCurveLinear,
                new SoundService.SoundData(Uri.parse(pathFolderSong), SoundService.MAX_VOLUME, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.LIGHT_SERVICE, LightFragment.LightData.SHARED_PREFERENCE, 1, duration_1, bezierCurveLinear,
                new LightFragment.LightData(LightFragment.MAX_BRIGHTNESS, LightFragment.MAX_BRIGHTNESS)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 1, duration_1, bezierCurve_1,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(0xffff7f00), Color.valueOf(Color.WHITE))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 1, duration_1, bezierCurve_1,
                new ClockFragment.ClockData(Color.valueOf(0xffBE4B00), Color.valueOf(Color.BLACK), Color.valueOf(0xffBE4B00), Color.valueOf(0xffff7f00), false, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 1, duration_1, bezierCurve_1,
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(0xffBE4B00), Color.valueOf(0x69d4d4d4)},
                        new Color[]{Color.valueOf(0xffBE4B00), Color.valueOf(0xffff7f00)},
                        false,
                        new Color[]{Color.valueOf(0xffff7f00), Color.valueOf(Color.BLACK)},
                        new Color[]{Color.valueOf(0xffff7f00), Color.valueOf(Color.BLACK)}
                )));

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.FLASH_SERVICE, FlashService.FlashData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear, new FlashService.FlashData()));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear,
                new SoundService.SoundData(Uri.parse(pathFolderSong), SoundService.MAX_VOLUME, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.LIGHT_SERVICE, LightFragment.LightData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear,
                new LightFragment.LightData(LightFragment.MAX_BRIGHTNESS, LightFragment.MAX_BRIGHTNESS)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear,
                new ClockFragment.ClockData(Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE), Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00), true, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 2, duration_2, bezierCurveLinear,
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(0x69d4d4d4), Color.valueOf(0x69d4d4d4)},
                        new Color[]{Color.valueOf(0xffff7f00), Color.valueOf(0xffff7f00)},
                        false,
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(Color.BLACK)},
                        new Color[]{Color.valueOf(Color.BLACK), Color.valueOf(Color.BLACK)}
                )));

        profileModel.setModuleList(moduleModels);

        return profileModel;
    }

    @Override
    public ProfileModel get(AlarmDatabase database) {
        return database.profileModel().getItemByNameSynchronous(name);
    }

    @Override
    public int count(AlarmDatabase database) {
        return database.profileModel().countItemByName(name);
    }

    @Override
    public String getName() { return name; }

    @Override
    public boolean needDirectoryAccess() {
        return true;
    }
}
