package com.octoton.wakeup.Profile.ProfileTemplate;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Color;
import android.net.Uri;

import com.octoton.wakeup.AlarmManager.ClockFragment;
import com.octoton.wakeup.AlarmManager.ColorGradientFragment;
import com.octoton.wakeup.AlarmManager.LightFragment;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.RepeatDismissFragment;
import com.octoton.wakeup.AlarmManager.SoundService;
import com.octoton.wakeup.AlarmManager.Utils.BezierCurve;
import com.octoton.wakeup.AlarmManager.Utils.Units;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.Database.ProfileModel;
import com.octoton.wakeup.R;

import java.util.ArrayList;
import java.util.List;

public class DefaultProfileTemplate implements ProfileTemplate {
    private final String name = "default";

    @Override
    public ProfileModel create(Activity activity) {
        ProfileModel profileModel;

        profileModel = new ProfileModel(name);

        List<ModuleModel> moduleModels = new ArrayList<>();

        // TODO: add preference view
        BezierCurve bezierCurve = new BezierCurve(0.3f, 0.0f, -1.2f, -0.0f);
        int duration_0 = 5 * Units.min; //use for snooze duration
        int duration_sound = 15 * Units.s;

        // TODO: order seems important, not a good limitation
        String rawSong = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(activity.getPackageName())
                .appendPath(String.valueOf(R.raw.oversimplified_alarm_clock))
                .build().toString();

        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.SOUND_SERVICE, SoundService.SoundData.SHARED_PREFERENCE, 0, duration_sound, bezierCurve,
                new SoundService.SoundData(Uri.parse(rawSong), 0, SoundService.MAX_VOLUME)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.COLOR_GRADIENT_SERVICE, ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new ColorGradientFragment.ColorGradientData(Color.valueOf(0xff0a6c58), Color.valueOf(0xff0a6c58))));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.CLOCK_SERVICE, ClockFragment.ClockData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                new ClockFragment.ClockData(Color.valueOf(0xff0a6c58), Color.valueOf(0xff0a6c58), Color.valueOf(0xff292444), Color.valueOf(0xff292444), true, false, false)));
        moduleModels.add(new ModuleModel(ModuleManager.MODULE_TAG.REPEAT_DISMISS_SERVICE, RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE, 0, duration_0, bezierCurve,
                //new RepeatDismissFragment.RepeatDismissData(context, true, false, true)));
                new RepeatDismissFragment.RepeatDismissData(true,
                        true,
                        false,
                        new Color[]{Color.valueOf(0x19efefef), Color.valueOf(0x19efefef)},
                        new Color[]{Color.valueOf(0xff292444), Color.valueOf(0xff292444)},
                        false,
                        new Color[]{Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE)},
                        new Color[]{Color.valueOf(Color.WHITE), Color.valueOf(Color.WHITE)}
                )));

        profileModel.setModuleList(moduleModels);

        return profileModel;
    }

    @Override
    public ProfileModel get(AlarmDatabase database) {
        return database.profileModel().getItemByNameSynchronous(name);
    }

    @Override
    public int count(AlarmDatabase database) {
        return database.profileModel().countItemByName(name);
    }

    @Override
    public String getName() { return name; }

    @Override
    public boolean needDirectoryAccess() {
        return false;
    }
}
