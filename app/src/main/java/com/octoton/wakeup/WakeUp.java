package com.octoton.wakeup;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.google.android.material.color.DynamicColors;

public class WakeUp extends Application {
    public static Toast alarmToast;

    @Override
    public void onCreate() {
        super.onCreate();
        //For enabling dynamic colors in the project
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        DynamicColors.applyToActivitiesIfAvailable(this);
    }

    public static void showAlarmToast(Context context, java.lang.CharSequence text, int duration) {
        if (alarmToast != null)
            alarmToast.cancel();

        alarmToast = Toast.makeText(context, text, duration);
        alarmToast.show();
    }
}