package com.octoton.wakeup;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import java.util.Calendar;
import java.util.Locale;

public class NotificationManager {
    private static final String msg = "Notification manager : ";
    public static final String CHANNEL_ID = "com.octoton.wakeup.Alarm.Notification";

    public static final String CHANNEL_ID_PREVIEW = "com.octoton.wakeup.Alarm_preview.Notification";
    public static final String CHANNEL_ID_INFO = "com.octoton.wakeup.Alarm_information.Notification";

    public enum ACTION {
        AlarmDefined,
        AlarmStop,
        AlarmSnooze,
        SnoozeStop,
        PreviewDefined,
        PreviewStop
    };

    public static String getPrintableDate(@Nullable Calendar calendar) {
        if (calendar == null) { return ""; }

        String day = new SimpleDateFormat("EE", Locale.getDefault()).format(calendar.getTime());
        String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(calendar.getTime());

        return day + " " + time;
    }

    public static int createRequestCode(int id, int day, int action) {
        int hash = 7;

        hash = hash * 31 + id;
        hash = hash * 31 + day;
        hash = hash * 31 + action;

        return hash;
    }

    public static void manageNotificationPermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission((Context) activity, android.Manifest.permission.POST_NOTIFICATIONS) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.POST_NOTIFICATIONS},101);
        }
    }

    public static void createNotificationChannel(Context context) {
        createNotificationChannel_Alarm(context);
        createNotificationChannel_Preview(context);
        createNotificationChannel_Info(context);
    }

    public static void createNotificationChannel_Alarm(Context context) {
        Log.d(msg, "Notification channel");
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        android.app.NotificationManager notificationManager = (android.app.NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = context.getString(R.string.channel_name);
        String description = context.getString(R.string.channel_desc);
        int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID,
                name,
                importance
        );
        serviceChannel.setSound(null, null);
        serviceChannel.setDescription(description);
        notificationManager.createNotificationChannel(serviceChannel);
        //}
    }

    public static void createNotificationChannel_Preview(Context context) {
        Log.d(msg, "Notification channel");
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        android.app.NotificationManager notificationManager = (android.app.NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = context.getString(R.string.channel_name_preview);
        String description = context.getString(R.string.channel_desc_preview);
        int importance = android.app.NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID_PREVIEW,
                name,
                importance
        );
        serviceChannel.setSound(null, null);
        serviceChannel.setDescription(description);
        notificationManager.createNotificationChannel(serviceChannel);
        //}
    }

    public static void createNotificationChannel_Info(Context context) {
        Log.d(msg, "Notification channel");
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        android.app.NotificationManager notificationManager = (android.app.NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        String name = context.getString(R.string.channel_name_info);
        String description = context.getString(R.string.channel_desc_info);
        int importance = android.app.NotificationManager.IMPORTANCE_LOW;
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID_INFO,
                name,
                importance
        );
        serviceChannel.setSound(null, null);
        serviceChannel.setDescription(description);
        notificationManager.createNotificationChannel(serviceChannel);
        //}
    }
}
