package com.octoton.wakeup.Notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.octoton.wakeup.AlarmManager.GraphicalModuleManager;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.DataModel.Synchronous.AlarmModelSynchronous;
import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.R;

import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class PreviewNotification {

    private static final int PREVIEW_ALARM_ID = -126;

    public static boolean set(Context context, int id, Calendar calendar) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id);
        if (alarmManager == null || alarmIntent == null) return false;

        Calendar previewCalendar = getCalendar(calendar);
        if (previewCalendar.after(Calendar.getInstance())) {
            removeAlarm(context, id);
            alarmManager.setWindow(AlarmManager.RTC, previewCalendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, alarmIntent);
        } else {
            /*AlarmModelSynchronous alarmModel = new AlarmModelSynchronous(context);
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                alarmModel.setPreview(id);
            });*/
            setNotification(context, id, calendar);
        }

        return true;
    }

    private static Calendar getCalendar(Calendar calendar) {
        Calendar previewCalendar = (Calendar)calendar.clone();
        Calendar forbiddenHourStart = (Calendar)calendar.clone();

        forbiddenHourStart.add(Calendar.DATE, -1);
        forbiddenHourStart.set(Calendar.HOUR_OF_DAY, 23-1); //TODO: preferences (-1 is to manage AlarmManager.INTERVAL_HOUR)
        forbiddenHourStart.set(Calendar.MINUTE, 0);

        Calendar forbiddenHourEnd = (Calendar)previewCalendar.clone();
        forbiddenHourEnd.set(Calendar.HOUR_OF_DAY, 10); //TODO: preferences
        forbiddenHourEnd.set(Calendar.MINUTE, 0);

        previewCalendar.add(Calendar.MILLISECOND, -(int) (2 * AlarmManager.INTERVAL_HOUR)); //TODO: preferences

        if (previewCalendar.before(forbiddenHourEnd))
            return forbiddenHourStart;
        else
            return previewCalendar;
    }

    public static boolean removeAlarm(Context context, int id) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id);
        if (alarmManager == null || alarmIntent == null) return false;

        alarmManager.cancel(alarmIntent);
        return true;
    }

    public static boolean removeNotification(Context context, int id) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(PREVIEW_ALARM_ID - id);
        return true;
    }

    public static void setNotification(Context context, int id, Calendar calendar) {
        android.app.NotificationManager mNotificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(PREVIEW_ALARM_ID - id, createNotification(context, id, calendar));
    }

    public static Notification createNotification(Context context, int id, Calendar calendar) {
        Intent cancelIntent = new Intent(context, AlarmReceiver.class);
        cancelIntent.setAction(AlarmReceiver.STOP_ALARM_FROM_PREVIEW);
        cancelIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, 0, NotificationManager.ACTION.PreviewStop.ordinal()), cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationManager.CHANNEL_ID_PREVIEW)
                .setSmallIcon(R.drawable.icon_foreground)
                .setContentTitle(context.getString(R.string.preview_title))
                .setContentText(NotificationManager.getPrintableDate(calendar))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_STATUS)
                .addAction(R.drawable.icon_foreground, context.getString(R.string.preview_dismiss),
                        cancelPendingIntent);

        return builder.build();
    }

    public static PendingIntent definedNotification(Context context, int id) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(AlarmReceiver.START_PREVIEW_ALARM);
        intent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        intent.putExtra(AlarmReceiver.CHANNEL_ID, NotificationManager.CHANNEL_ID);

        return PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, 0, NotificationManager.ACTION.PreviewDefined.ordinal()), intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }
}
