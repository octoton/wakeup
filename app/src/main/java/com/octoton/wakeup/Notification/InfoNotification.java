package com.octoton.wakeup.Notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.Preference.SettingsActivity;
import com.octoton.wakeup.R;

public class InfoNotification {

    public static void showNotificationOrientationUndetected(Context context) {
        Intent resultIntent = new Intent(context, SettingsActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack.
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationManager.CHANNEL_ID_INFO)
                .setSmallIcon(R.drawable.icon_foreground)
                .setContentTitle("Devices orientation could not be determined")
                .setContentText("See settings to choose a default orientation")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_ERROR)
                .setContentIntent(resultPendingIntent);

        android.app.NotificationManager mNotificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify((int) SystemClock.uptimeMillis(), builder.build());
    }
}
