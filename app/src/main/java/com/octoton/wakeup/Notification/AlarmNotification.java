package com.octoton.wakeup.Notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.octoton.wakeup.AlarmManager.GraphicalModuleManager;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.R;

import java.util.Calendar;

public class AlarmNotification {

    public static boolean updateAlarm(Context context, AlarmModel alarmModel, AlarmModel oldAlarmModel) {
        Calendar calendar = (oldAlarmModel == null) ? alarmModel.getCalendar() : oldAlarmModel.getCalendar();
        Calendar nextCalendar = (oldAlarmModel == null) ? alarmModel.getNextCalendar() : oldAlarmModel.getNextCalendar();
        if (removeAlarm(context, alarmModel.id, calendar, nextCalendar)) {
            return AlarmNotification.setAlarmOrNextIfIgnored(context, alarmModel);
        } else {
            return false;
        }
    }

    public static boolean setAlarmOrNextIfIgnored(Context context, AlarmModel alarmModel) {
        if (!alarmModel.hasItBeenIgnored()) {
            boolean hasPreviewAlreadyBeenShown = alarmModel.hasPreviewAlreadyBeenShown();
            if (!hasPreviewAlreadyBeenShown) alarmModel.showPreview(); // TODO: only if preview is really shown
            return setAlarm(context, alarmModel.id, alarmModel.getCalendar(), alarmModel.hasItBeenIgnored(), hasPreviewAlreadyBeenShown);
        } else {
            alarmModel.showPreview(); // TODO: only if preview is really shown
            return setAlarm(context, alarmModel.id, alarmModel.getNextCalendar(), false, false);
        }
    }

    public static boolean setAlarm(Context context, int id, @Nullable Calendar calendar, boolean isIgnored, boolean previewDone) {
        if (calendar == null) return false;
        if (isIgnored) return false;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id, 0, calendar.get(Calendar.DAY_OF_MONTH));
        if (alarmManager == null || alarmIntent == null) return false;

        if (!previewDone) PreviewNotification.set(context, id, calendar);
        alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), null), alarmIntent);
        return true;
    }

    public static boolean removeAlarm(Context context, int id, Calendar calendar, Calendar nextCalendar) {
        if (nextCalendar != null) removeAlarm(context, id, 0, nextCalendar.get(Calendar.DAY_OF_MONTH)); // TODO: only useful if I was registering 2 alarm at a time
        return removeAlarm(context, id, 0, calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static boolean removeAlarm(Context context, int id, int stepNumber, int stepDay) {
        // Alarm
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id, stepNumber, stepDay);
        if (alarmManager == null || alarmIntent == null) return false;
        if (stepNumber == 0) PreviewNotification.removeAlarm(context, id);
        alarmManager.cancel(alarmIntent);
        return true;
    }

    public static boolean removeNotification(Context context, int notificationId) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(notificationId);
        return true;
    }

    public static Notification createNotification(Context context, int id, int day) {
        PreviewNotification.removeNotification(context, id);

        Calendar calendar = Calendar.getInstance();

        Intent fullScreenIntent = new Intent(context, GraphicalModuleManager.class);
        fullScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        fullScreenIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        //fullScreenIntent.addFlags(Notification.FLAG_NO_CLEAR); //below api21

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 50,
                fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Intent cancelIntent = new Intent(context, AlarmReceiver.class);
        cancelIntent.setAction(AlarmReceiver.STOP_ALARM);
        cancelIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, day, NotificationManager.ACTION.AlarmStop.ordinal()), cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Intent snoozeIntent = new Intent(context, AlarmReceiver.class);
        snoozeIntent.setAction(AlarmReceiver.SNOOZE_ALARM);
        snoozeIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        PendingIntent snoozePendingIntent = PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, day, NotificationManager.ACTION.AlarmSnooze.ordinal()), snoozeIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationManager.CHANNEL_ID)
                .setSmallIcon(R.drawable.icon_foreground)
                .setContentTitle(context.getString(R.string.alarm_title))
                .setContentText(NotificationManager.getPrintableDate(calendar))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setForegroundServiceBehavior(android.app.Notification.FOREGROUND_SERVICE_IMMEDIATE) // TODO: remove when notification got button
                .setDeleteIntent(cancelPendingIntent)
                .setFullScreenIntent(fullScreenPendingIntent, true)
                .addAction(R.drawable.icon_foreground, context.getString(R.string.snooze),
                        snoozePendingIntent)
                .addAction(R.drawable.icon_foreground, context.getString(R.string.cancel),
                        cancelPendingIntent);

        return builder.build();
    }

    public static PendingIntent definedNotification(Context context, int id, int stepNumber, int stepDay) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(AlarmReceiver.START_ALARM);
        intent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        intent.putExtra(AlarmReceiver.STEP_NUMBER, stepNumber);
        intent.putExtra(AlarmReceiver.STEP_DAY, stepDay);
        intent.putExtra(AlarmReceiver.CHANNEL_ID, NotificationManager.CHANNEL_ID);

        return PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, stepDay, NotificationManager.ACTION.AlarmDefined.ordinal()), intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }
}
