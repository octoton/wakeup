package com.octoton.wakeup.Notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.octoton.wakeup.AlarmManager.Utils.Units;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.NotificationManager;
import com.octoton.wakeup.R;

import java.util.Calendar;

public class SnoozeNotification {

    public static boolean set(Context context, int id, int stepNumber, int day, int durationInMilli, int notificationId) {
        // Alarm
        int timeBeforeSnoozeInMinute = (int) Math.ceil((float) durationInMilli/ (float) Units.min );

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, timeBeforeSnoozeInMinute);
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id, stepNumber, day);
        if (alarmManager == null || alarmIntent == null) return false;

        alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), null), alarmIntent);

        // Notification
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, createNotification(context, id, stepNumber, day, calendar, timeBeforeSnoozeInMinute));

        return true;
    }

    public static boolean remove(Context context, int id, int stepNumber, int day, int notificationId) {
        // Alarm
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = definedNotification(context, id, stepNumber, day);
        if (alarmManager == null || alarmIntent == null) return false;

        alarmManager.cancel(alarmIntent);

        // Notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(notificationId);

        return true;
    }

    public static Notification createNotification(Context context, int id, int stepNumber, int stepDay, Calendar calendar, int timeBeforeSnoozeInMinute) {
        Intent cancelIntent = new Intent(context, AlarmReceiver.class);
        cancelIntent.setAction(AlarmReceiver.STOP_ALARM);
        cancelIntent.putExtra(AlarmReceiver.STEP_DAY, stepDay);
        cancelIntent.putExtra(AlarmReceiver.STEP_NUMBER, stepNumber);
        cancelIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, id);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, NotificationManager.createRequestCode(id, stepDay, NotificationManager.ACTION.SnoozeStop.ordinal()), cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationManager.CHANNEL_ID)
                .setSmallIcon(R.drawable.icon_foreground)
                .setContentTitle(context.getString(R.string.snooze_title, timeBeforeSnoozeInMinute))
                .setContentText(NotificationManager.getPrintableDate(calendar))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setForegroundServiceBehavior(android.app.Notification.FOREGROUND_SERVICE_IMMEDIATE) // TODO: remove when notification got button
                .setDeleteIntent(cancelPendingIntent)
                .addAction(R.drawable.icon_foreground, context.getString(R.string.snooze_dismiss),
                        cancelPendingIntent);

        return builder.build();
    }

    public static PendingIntent definedNotification(Context context, int id, int stepNumber, int stepDay) {
        return AlarmNotification.definedNotification(context, id, stepNumber, stepDay);
    }
}
