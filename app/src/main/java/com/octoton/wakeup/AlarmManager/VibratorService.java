package com.octoton.wakeup.AlarmManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.os.VibratorManager;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;

public class VibratorService implements Module {
    static String msg = "Vibrator service : ";

    Vibrator vibrator;

    VibratorData data;

    @Override
    public Integer getParentId() {
        return null;
    }

    final int DELAY = 0, VIBRATE = 1000, SLEEP = 1000, START = 0;
    long[] vibratePattern = {DELAY, VIBRATE, SLEEP};

    @Override
    public void start(int position) {
        //TODO: not working when device in low battery mode
        //vibratorManager.vibrate(CombinedVibration.createParallel(VibrationEffect.startComposition().addPrimitive(VibrationEffect.Composition.PRIMITIVE_THUD).compose()));
        vibrator.vibrate(VibrationEffect.createWaveform(vibratePattern, START));
    }

    @Override
    public void stop() {
        vibrator.cancel();
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step) {
        load(context, sharedPreferences, alarmModel, moduleModel);
    }

    public static VibratorData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        VibratorData data = new VibratorData();

        data.name = VibratorData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);

        //if api 21
        VibratorManager vibratorManager = (VibratorManager) context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE);
        //context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator = vibratorManager.getDefaultVibrator();
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
    }

    @Override
    public void activityRegisteredCLick() {

    }

    @Override
    public Data getData() {
        return data;
    }


    public static class VibratorData extends Module.Data {
        public static String SHARED_PREFERENCE = "vibrator_data_shared-preference";

        public VibratorData() {}

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            return true;
        }
    }
}
