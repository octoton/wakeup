package com.octoton.wakeup.AlarmManager;

import android.animation.ArgbEvaluator;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.Fragment;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.ColorEvaluator;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.BezierView;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.R;

public class ColorGradientFragment extends Fragment implements Module {
    String msg = "Color gradient Fragment : ";
    int parentId;

    float amount;
    LinearLayout view;

    ValueAnimator colorAnimation;

    ColorGradientData data;

    public ColorGradientFragment(int parentId) {
        super(R.layout.fragment_color_gradient);
        this.parentId = parentId;
    }

    @Override
    public Integer getParentId() {
        return parentId;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getView() == null) { return; }

        Log.d(msg, "Start");
        view = (LinearLayout)getView().findViewById(R.id.background);

        colorAnimation = ValueAnimator.ofObject(new ColorEvaluator(), data.startColor, data.targetColor);
        colorAnimation.setInterpolator(PathInterpolatorCompat.create(data.path));
        colorAnimation.setDuration(data.duration); // milliseconds
        colorAnimation.setCurrentFraction(amount);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                view.setBackgroundColor(((Color) animator.getAnimatedValue()).toArgb());
            }

        });
        colorAnimation.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        stop();
    }

    @Override
    public void activityRegisteredCLick() {

    }

    @Override
    public void start(int position) {
        amount = Math.min((float) position / data.duration, 1.0f);
    }

    @Override
    public void stop() {
        colorAnimation.cancel();
    }

    public static ColorGradientData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        ColorGradientData data = new ColorGradientData();

        data.name = ColorGradientData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        data.startColor = Color.valueOf(sharedPreferences.getInt(ColorGradientData.STARTCOLOR, Color.BLACK));
        data.targetColor = Color.valueOf(sharedPreferences.getInt(ColorGradientData.TARGETCOLOR, Color.GREEN)); //0xffff7f00));

        return data;
    }
    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step)
    {
        stop();
        load(context, sharedPreferences, alarmModel, moduleModel);
        start(step.currentDuration);
        onStart();
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
        //Module.write(editor, data);
        editor.putInt(ColorGradientData.STARTCOLOR, ((ColorGradientData)data).startColor.toArgb());
        editor.putInt(ColorGradientData.TARGETCOLOR, ((ColorGradientData)data).targetColor.toArgb());
    }

    public Data getData() {
        return data;
    }

    public static class ColorGradientData extends Module.Data {
        public static String SHARED_PREFERENCE = "color_gradient_data_shared-preference";
        public static String STARTCOLOR = "start color";
        public static String TARGETCOLOR = "target color";

        public Color startColor;
        public Color targetColor;

        public ColorGradientData() {}
        public ColorGradientData(Color startColor, Color targetColor) {
            this.startColor = startColor;
            this.targetColor = targetColor;
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            ColorGradientData data = (ColorGradientData) object;
            if (!this.startColor.equals(data.startColor)) return false;
            if (!this.targetColor.equals(data.targetColor)) return false;

            return true;
        }
    }
}
