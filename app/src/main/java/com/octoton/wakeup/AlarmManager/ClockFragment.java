package com.octoton.wakeup.AlarmManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.Fragment;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.ColorEvaluator;
import com.octoton.wakeup.AlarmManager.Utils.SizeAwareTextClock;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.R;

import java.util.Calendar;
import java.util.Date;

public class ClockFragment extends Fragment implements Module {
    String msg = "Clock Fragment : ";
    int parentId;

    ClockData data;

    View rootView;
    SizeAwareTextClock clock;
    RelativeLayout extra;
    TextView title;
    TextView day;
    ValueAnimator colorAnimation;
    ColorEvaluator colorConverter;
    float amount;
    String titleAlarm;

    public ClockFragment(int parentId) {
        super();

        this.parentId = parentId;
    }

    @Override
    public Integer getParentId() {
        return parentId;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_clock, container, false);

        clock = rootView.findViewById(R.id.clock);

        extra = rootView.findViewById(R.id.extra);
        title = rootView.findViewById(R.id.title);
        day = rootView.findViewById(R.id.day);

        clock.setVisibility(View.GONE);
        title.setVisibility(View.GONE);
        day.setVisibility(View.GONE);
        if (!data.clickToDismiss) {
            setClockVisibility(true);
        }

        setExtra();

        setColorAnimation();

        SizeAwareTextClock.OnClockSizeChangedListener onClockSizeChangedListener = new SizeAwareTextClock.OnClockSizeChangedListener() {
            @Override
            public void onClockSizeChanged(SizeAwareTextClock view, float textSize) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    int pxValue = (int) (clock.getHeight() - textSize) / 2 + (int) getResources().getDimension(R.dimen.separator);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) day.getLayoutParams();
                    layoutParams.setMargins(layoutParams.leftMargin, pxValue, layoutParams.rightMargin, layoutParams.bottomMargin);
                    day.setLayoutParams(layoutParams);
                } else {
                    int pxValue = (int)textSize + (int) getResources().getDimension(R.dimen.textSmall);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) extra.getLayoutParams();
                    layoutParams.setMargins(layoutParams.leftMargin, pxValue, layoutParams.rightMargin, layoutParams.bottomMargin);
                    extra.setLayoutParams(layoutParams);
                }
            }
        };
        clock.setOnClockSizeChangedListener(onClockSizeChangedListener);

        return rootView;
    }

    private void setExtra() {
        Date date = Calendar.getInstance().getTime();
        String dayWithNumber = android.text.format.DateFormat.format("EEE dd", date).toString();
        day.setText( capitalizeFirstLetter(dayWithNumber) );

        title.setText( capitalizeFirstLetter(this.titleAlarm) );
    }

    private String capitalizeFirstLetter(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        char firstChar = Character.toUpperCase(text.charAt(0));
        return firstChar + text.substring(1);
    }

    @Override
    public void onStart() { super.onStart(); }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void activityRegisteredCLick() {
        if (data.clickToDismiss) {
            setClockVisibility(true);
        }
    }

    @Override
    public void start(int position) {
        amount = Math.min((float) position / data.duration, 1.0f);
    }

    @Override
    public void stop() {
        colorAnimation.cancel();
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step)
    {
        stop();
        load(context, sharedPreferences, alarmModel, moduleModel);
        start(step.currentDuration);

        setExtra();

        setClockVisibility(!data.clickToDismiss);
        setColorAnimation();
    }

    private void setClockVisibility(boolean showClock) {
        float a0, a1;
        if (showClock) {
            a0 = 0f; a1 = 1f;
        } else {
            a0 = 1f; a1 = 0f;
        }
        ObjectAnimator alphaAnimatorClock = ObjectAnimator.ofFloat(
                rootView, "alpha", a0, a1
        );

        if (showClock) {
            alphaAnimatorClock.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    clock.setVisibility(View.VISIBLE);
                    title.setVisibility(View.VISIBLE);
                    day.setVisibility(View.VISIBLE);
                }
            });
        } else {
            alphaAnimatorClock.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    clock.setVisibility(View.VISIBLE);
                    title.setVisibility(View.VISIBLE);
                    day.setVisibility(View.VISIBLE);
                }
                @Override
                public void onAnimationEnd(Animator animation) {
                    clock.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                    day.setVisibility(View.GONE);
                }
            });
        }

        int animationDuration = 0;
        if ((showClock && clock.getVisibility()!=View.VISIBLE) ||
                (!showClock && clock.getVisibility()==View.VISIBLE)) { animationDuration = 500; } // Duration in milliseconds
        alphaAnimatorClock.setDuration(animationDuration);

        alphaAnimatorClock.start();
    }


    private void setColorAnimation() {
        colorConverter = new ColorEvaluator();

        colorAnimation = ValueAnimator.ofObject(new FloatEvaluator(), 0.0f, 1.0f);
        colorAnimation.setInterpolator(PathInterpolatorCompat.create(data.path));
        colorAnimation.setDuration(data.duration); // milliseconds
        colorAnimation.setCurrentFraction(amount);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (getContext() == null) { return; }
                float animatorValue = (float) animator.getAnimatedValue();

                int colorPrimary = ((Color) colorConverter.evaluate(animatorValue, data.startPrimaryColor, data.targetPrimaryColor)).toArgb();
                if (!data.automaticContrast) {
                    clock.setTextColor(colorPrimary);
                    title.setTextColor(colorPrimary);
                } else {
                    int contrastColorPrimary = ColorUtils.calculateContrast(Color.WHITE, ColorEvaluator.stripAlpha(colorPrimary)) >= 4.5
                            ? Color.WHITE
                            : Color.BLACK;

                    clock.setTextColor(contrastColorPrimary);
                    title.setTextColor(contrastColorPrimary);
                }

                int colorSecondary = ((Color) colorConverter.evaluate(animatorValue, data.startSecondaryColor, data.targetSecondaryColor)).toArgb();
                day.setTextColor(colorSecondary);
            }

        });
        colorAnimation.start();
    }

    public static ClockData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        ClockData data = new ClockData();

        data.name = ClockData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        data.startPrimaryColor = Color.valueOf(sharedPreferences.getInt(ClockData.STARTPRIMARYCOLOR, Color.WHITE));
        data.targetPrimaryColor = Color.valueOf(sharedPreferences.getInt(ClockData.TARGETPRIMARYCOLOR, Color.WHITE));
        data.startSecondaryColor = Color.valueOf(sharedPreferences.getInt(ClockData.STARTSECONDARYCOLOR, Color.RED));
        data.targetSecondaryColor = Color.valueOf(sharedPreferences.getInt(ClockData.TARGETSECONDARYCOLOR, Color.RED));
        data.automaticContrast = sharedPreferences.getBoolean(ClockData.AUTOMATICCONTRAST, false);
        data.clickToDismiss = sharedPreferences.getBoolean(ClockData.CLICKTODISMISS, true);
        data.fullScreen = sharedPreferences.getBoolean(ClockData.FULLSCREEN, false);

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);

        this.titleAlarm = alarmModel.getName();
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
        editor.putInt(ClockData.STARTPRIMARYCOLOR, ((ClockData)data).startPrimaryColor.toArgb());
        editor.putInt(ClockData.TARGETPRIMARYCOLOR, ((ClockData)data).targetPrimaryColor.toArgb());
        editor.putInt(ClockData.STARTSECONDARYCOLOR, ((ClockData)data).startSecondaryColor.toArgb());
        editor.putInt(ClockData.TARGETSECONDARYCOLOR, ((ClockData)data).targetSecondaryColor.toArgb());
        editor.putBoolean(ClockData.AUTOMATICCONTRAST, ((ClockData)data).automaticContrast);
        editor.putBoolean(ClockData.CLICKTODISMISS, ((ClockData)data).clickToDismiss);
        editor.putBoolean(ClockData.FULLSCREEN, ((ClockData)data).fullScreen);
    }

    public Data getData() {
        return data;
    }

    public static class ClockData extends Module.Data {
        public static String SHARED_PREFERENCE = "clock_data_shared-preference";
        public static String STARTPRIMARYCOLOR = "start primary color";
        public static String TARGETPRIMARYCOLOR = "target primary color";
        public static String STARTSECONDARYCOLOR = "start secondary color";
        public static String TARGETSECONDARYCOLOR = "target secondary color";
        public static String AUTOMATICCONTRAST = "automatic contrast";
        public static String CLICKTODISMISS = "click to dismiss";
        public static String FULLSCREEN = "fullscreen";

        public Color startPrimaryColor;
        public Color targetPrimaryColor;
        public Color startSecondaryColor;
        public Color targetSecondaryColor;
        public boolean automaticContrast;
        public Boolean clickToDismiss;
        public Boolean fullScreen;

        public ClockData() {}

        public ClockData(Context context, Color startPrimaryColor, Color targetPrimaryColor, boolean automaticContrast, boolean clickToDismiss, boolean fullScreen) {
                this(startPrimaryColor, targetPrimaryColor,
                        Color.valueOf(ContextCompat.getColor(context, R.color.cancel)), Color.valueOf(ContextCompat.getColor(context, R.color.cancel)),
                        automaticContrast, clickToDismiss, fullScreen);
        }

        public ClockData(Color startPrimaryColor, Color targetPrimaryColor, Color startSecondaryColor, Color targetSecondaryColor, boolean automaticContrast, boolean clickToDismiss, boolean fullScreen) {
            this.startPrimaryColor = startPrimaryColor;
            this.targetPrimaryColor = targetPrimaryColor;
            this.startSecondaryColor = startSecondaryColor;
            this.targetSecondaryColor = targetSecondaryColor;
            this.automaticContrast = automaticContrast;
            this.clickToDismiss = clickToDismiss;
            this.fullScreen = fullScreen;
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            ClockData data = (ClockData) object;
            if (!this.startPrimaryColor.equals(data.startPrimaryColor)) return false;
            if (!this.targetPrimaryColor.equals(data.targetPrimaryColor)) return false;
            if (!this.startSecondaryColor.equals(data.startSecondaryColor)) return false;
            if (!this.targetSecondaryColor.equals(data.targetSecondaryColor)) return false;
            if (this.automaticContrast != data.automaticContrast) return false;
            if (!this.clickToDismiss.equals(data.clickToDismiss)) return false;
            if (!this.fullScreen.equals(data.fullScreen)) return false;

            return true;
        }
    }
}
