package com.octoton.wakeup.AlarmManager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowMetrics;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.Fragment;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.AlarmListener;
import com.octoton.wakeup.AlarmManager.Utils.ColorEvaluator;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.R;

import java.util.Arrays;

public class RepeatDismissFragment extends Fragment implements Module {
    String msg = "RepeatDismiss Fragment : ";
    int parentId;
    Button repeat;
    Button dismiss;
    ColorEvaluator colorConverter;
    ValueAnimator colorAnimation;
    float amount;
    RepeatDismissData data;
    AnimatedVectorDrawable repeatAnimation;
    Animatable2.AnimationCallback repeatCallback;
    AnimatedVectorDrawable dismissAnimation;
    Animatable2.AnimationCallback dismissCallback;
    Animation shake;
    boolean isAnimating;

    public RepeatDismissFragment(int parentId) {
        super();
        this.parentId = parentId;
    }

    @Override
    public Integer getParentId() {
        return parentId;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_repeat_dismiss, container, false);

        Log.d(msg, "oncreateview");
        repeat = (Button) rootView.findViewById(R.id.repeat);
        repeat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((AlarmListener)requireActivity()).onRepeat();
            }
        });

        dismiss = (Button) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((AlarmListener)requireActivity()).onCancel();
            }
        });

        repeat.setForeground(ResourcesCompat.getDrawable(getResources(), R.drawable.animated_rounded_square, null));
        repeatAnimation = (AnimatedVectorDrawable) repeat.getForeground();
        repeatCallback = new Animatable2.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                repeatAnimation.reset();
                if (!isAnimating) return;

                if (data.dismiss)
                    dismiss.startAnimation(shake); //dismissAnimation.start();
                else
                    repeatAnimation.start();
            }
        };
        repeatAnimation.registerAnimationCallback(repeatCallback);

        dismiss.setForeground(ResourcesCompat.getDrawable(getResources(), R.drawable.animated_circle, null));
        dismissAnimation = (AnimatedVectorDrawable) dismiss.getForeground();
        dismissCallback = new Animatable2.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                dismissAnimation.reset();
                if (!isAnimating) return;

                if (data.repeat)
                    repeatAnimation.start();
                else
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dismissAnimation.start();
                        }
                    }, 2000);
            }
        };
        dismissAnimation.registerAnimationCallback(dismissCallback);

        shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {}
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!isAnimating) return;

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (data.repeat)
                            repeatAnimation.start();
                        else
                            dismiss.startAnimation(shake);
                    }}, 1500);
            }
        });

        repeat.setVisibility(View.GONE);
        dismiss.setVisibility(View.GONE);
        resetButtonAnimation();
        if (!data.clickToDismiss)
            setVisibilityOfButtonWithAnimation(data.repeat, data.dismiss);

        setColorAnimation();

        return rootView;
    }

    private void resetButtonAnimation() {
        isAnimating = false;
        repeatAnimation.reset();
        dismissAnimation.reset();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(msg, "onstart");
    }

    @Override
    public void activityRegisteredCLick() {
        if (data.clickToDismiss)
            setVisibilityOfButtonWithAnimation(data.repeat, data.dismiss);
    }

    private int getRepeatVisibleSize() {
        WindowMetrics windowMetrics = requireActivity().getWindowManager().getCurrentWindowMetrics();

        int screenSize;
        screenSize = windowMetrics.getBounds().width();

        return screenSize - 2*(int)getResources().getDimension(R.dimen.separator) - (int)getResources().getDimension(R.dimen.spacing) - getDismissVisibleSize();
    }

    private int getDismissVisibleSize() {
        return (int)getResources().getDimension(R.dimen.cancelSize);
    }

    private void setButtonSize(ValueAnimator animation, Button button, int buttonVisibleSize) {
        int newSize = (int) animation.getAnimatedValue();
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.width = newSize;

        // margin
        int pxValue = (int)(getResources().getDimension(R.dimen.smalling) * (float) (newSize) / buttonVisibleSize );

        layoutParams.setMargins(pxValue, layoutParams.topMargin, pxValue, layoutParams.bottomMargin);
        button.setLayoutParams(layoutParams);
    }

    private void setVisibilityOfButtonWithAnimation(boolean showRepeat, boolean showDismiss) {
        if (!showRepeat && !showDismiss)
            resetButtonAnimation();

        setButtonVisibility(showRepeat, repeat, getRepeatVisibleSize(), () -> { if (!isAnimating && showRepeat) {isAnimating = true; repeatAnimation.start();} });
        setButtonVisibility(showDismiss, dismiss, getDismissVisibleSize(), () -> { if (!isAnimating && showDismiss && !showRepeat) {isAnimating = true; dismissAnimation.start();} });
    }

    private void setButtonVisibility(boolean show, Button button, int buttonVisibleSize, Runnable start) {
        int x0, x1;
        float a0, a1;
        if (show) {
            x0 = 0; x1 = buttonVisibleSize;
            a0 = 0f; a1 = 1f;
        } else {
            x0 = buttonVisibleSize; x1 = 0;
            a0 = 1f; a1 = 0f;
        }
        ValueAnimator scaleXAnimator = ValueAnimator.ofInt(x0, x1);
        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(
                button, "alpha", a0, a1
        );

        if (show) {
            alphaAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    button.setVisibility(View.VISIBLE);
                }
                @Override
                public void onAnimationEnd(Animator animation) {
                    if (start != null) start.run();
                }
            });
        } else {
            alphaAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    button.setVisibility(View.VISIBLE);
                }
                @Override
                public void onAnimationEnd(Animator animation) {
                    button.setVisibility(View.GONE);
                }
            });
        }

        scaleXAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //TODO: when bezier curve is too steep (BezierCurve(0.1f, 0.8f, -0.8f, -0.1f) cancel button image is squashed (but button is round)
                setButtonSize(animation, button, buttonVisibleSize);
            }
        });

        AnimatorSet animatorSetRepeat = new AnimatorSet();
        animatorSetRepeat.playTogether(scaleXAnimator, alphaAnimator);

        int animationDuration = 0;
        if ((show && button.getVisibility()!=View.VISIBLE) ||
                (!show && button.getVisibility()==View.VISIBLE)) { animationDuration = 500; } // Duration in milliseconds
        animatorSetRepeat.setDuration(animationDuration);

        animatorSetRepeat.start();
    }

    private void setColorAnimation() {
        colorConverter = new ColorEvaluator();

        colorAnimation = ValueAnimator.ofObject(new FloatEvaluator(), 0.0f, 1.0f);
        colorAnimation.setInterpolator(PathInterpolatorCompat.create(data.path));
        colorAnimation.setDuration(data.duration); // milliseconds
        colorAnimation.setCurrentFraction(amount);

        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (getContext() == null) { return; }
                float animatorValue = (float) animator.getAnimatedValue();

                Color backgroundColorRepeat = (Color) colorConverter.evaluate(animatorValue, data.repeatColor[0], data.repeatColor[1]);
                repeat.setBackgroundTintList(ColorStateList.valueOf(backgroundColorRepeat.toArgb())); //TODO: is not right when testing (compare to clock widget)

                Color backgroundColorDismiss = (Color) colorConverter.evaluate(animatorValue, data.dismissColor[0], data.dismissColor[1]);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dismiss.getLayoutParams();
                Drawable drawable = AppCompatResources.getDrawable(requireActivity(), R.drawable.baseline_cancel_square_24).mutate();
                DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
                drawable.setBounds(0, 0, layoutParams.width, layoutParams.height); //TODO: is lagging behind xTransform + don't understand why this is working when click to dismiss
                                                                                             // sometime it is stuck midway
                DrawableCompat.setTint(drawable, backgroundColorDismiss.toArgb());
                dismiss.setCompoundDrawables(drawable, null, null, null);

                int colorRepeatText = ((Color) colorConverter.evaluate(animatorValue, data.repeatTextColor[0], data.repeatTextColor[1])).toArgb();
                int colorDismissIcon = ((Color) colorConverter.evaluate(animatorValue, data.dismissTextColor[0], data.dismissTextColor[1])).toArgb();
                if (!data.automaticContrast) {
                    repeat.setTextColor(colorRepeatText);
                    dismiss.setBackgroundTintList(ColorStateList.valueOf(colorDismissIcon));
                } else {
                    int contrastColorRepeat;
                    if (backgroundColorRepeat.alpha() >= 0.5f) {
                        contrastColorRepeat = ColorUtils.calculateContrast(Color.WHITE, ColorEvaluator.stripAlpha(backgroundColorRepeat.toArgb())) >= 4.5
                                ? Color.WHITE
                                : Color.BLACK;
                    } else {
                        contrastColorRepeat = ColorUtils.calculateContrast(Color.WHITE, ColorEvaluator.stripAlpha(colorRepeatText)) >= 4.5
                                ? Color.WHITE
                                : Color.BLACK;
                    }
                    repeat.setTextColor(contrastColorRepeat);

                    int contrastColorDismiss;
                    if (backgroundColorDismiss.alpha() >= 0.5f) {
                        contrastColorDismiss = ColorUtils.calculateContrast(Color.WHITE, ColorEvaluator.stripAlpha(backgroundColorDismiss.toArgb())) >= 4.5
                                ? Color.WHITE
                                : Color.BLACK;
                    } else {
                        contrastColorDismiss = ColorUtils.calculateContrast(Color.WHITE, ColorEvaluator.stripAlpha(colorDismissIcon)) >= 4.5
                                ? Color.WHITE
                                : Color.BLACK;
                    }
                    dismiss.setBackgroundTintList(ColorStateList.valueOf(contrastColorDismiss));
                }
            }

        });
        colorAnimation.start();
    }


    @Override
    public void onStop() {
        super.onStop();

        repeatAnimation.unregisterAnimationCallback(repeatCallback);
        dismissAnimation.unregisterAnimationCallback(dismissCallback);

        Log.d(msg, "onstop");
    }

    @Override
    public void start(int position) {
        amount = Math.min((float) position / data.duration, 1.0f);
    }

    @Override
    public void stop() {
        colorAnimation.cancel();
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step)
    {
        stop();
        load(context, sharedPreferences, alarmModel, moduleModel);

        if (data.clickToDismiss) {
            setVisibilityOfButtonWithAnimation(false, false);
        } else {
            setVisibilityOfButtonWithAnimation(data.repeat, data.dismiss);
        }

        start(0);
        setColorAnimation();
    }

    public static RepeatDismissData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        RepeatDismissData data = new RepeatDismissData();

        data.name = RepeatDismissData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        data.repeat = sharedPreferences.getBoolean(RepeatDismissData.REPEAT, false);
        data.dismiss = sharedPreferences.getBoolean(RepeatDismissData.DISMISS, true);
        data.clickToDismiss = sharedPreferences.getBoolean(RepeatDismissData.CLICKTODISMISS, false);

        data.repeatColor = new Color[2];
        data.repeatColor[0] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.START_REPEATCOLOR, ContextCompat.getColor(context, R.color.snooze)));
        data.repeatColor[1] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.END_REPEATCOLOR, ContextCompat.getColor(context, R.color.snooze)));

        data.dismissColor = new Color[2];
        data.dismissColor[0] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.START_DISMISSCOLOR, ContextCompat.getColor(context, R.color.cancel)));
        data.dismissColor[1] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.END_DISMISSCOLOR, ContextCompat.getColor(context, R.color.cancel)));

        data.automaticContrast = sharedPreferences.getBoolean(RepeatDismissData.AUTOMATICCONTRAST, false);

        data.repeatTextColor = new Color[2];
        data.repeatTextColor[0] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.START_REPEATTEXTCOLOR, ContextCompat.getColor(context, R.color.white)));
        data.repeatTextColor[1] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.END_REPEATTEXTCOLOR, ContextCompat.getColor(context, R.color.white)));

        data.dismissTextColor = new Color[2];
        data.dismissTextColor[0] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.START_DISMISSTEXTCOLOR, ContextCompat.getColor(context, R.color.white)));
        data.dismissTextColor[1] = Color.valueOf(sharedPreferences.getInt(RepeatDismissData.END_DISMISSTEXTCOLOR, ContextCompat.getColor(context, R.color.white)));

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
        editor.putBoolean(RepeatDismissData.REPEAT, ((RepeatDismissData)data).repeat);
        editor.putBoolean(RepeatDismissData.DISMISS, ((RepeatDismissData)data).dismiss);
        editor.putBoolean(RepeatDismissData.CLICKTODISMISS, ((RepeatDismissData)data).clickToDismiss);
        editor.putInt(RepeatDismissData.START_REPEATCOLOR, ((RepeatDismissData)data).repeatColor[0].toArgb());
        editor.putInt(RepeatDismissData.END_REPEATCOLOR, ((RepeatDismissData)data).repeatColor[1].toArgb());
        editor.putInt(RepeatDismissData.START_DISMISSCOLOR, ((RepeatDismissData)data).dismissColor[0].toArgb());
        editor.putInt(RepeatDismissData.END_DISMISSCOLOR, ((RepeatDismissData)data).dismissColor[1].toArgb());

        editor.putBoolean(RepeatDismissData.AUTOMATICCONTRAST, ((RepeatDismissData)data).automaticContrast);

        editor.putInt(RepeatDismissData.START_REPEATTEXTCOLOR, ((RepeatDismissData)data).repeatTextColor[0].toArgb());
        editor.putInt(RepeatDismissData.END_REPEATTEXTCOLOR, ((RepeatDismissData)data).repeatTextColor[1].toArgb());
        editor.putInt(RepeatDismissData.START_DISMISSTEXTCOLOR, ((RepeatDismissData)data).dismissTextColor[0].toArgb());
        editor.putInt(RepeatDismissData.END_DISMISSTEXTCOLOR, ((RepeatDismissData)data).dismissTextColor[1].toArgb());
    }

    public Data getData() {
        return data;
    }

    public static class RepeatDismissData extends Module.Data {
        public static String SHARED_PREFERENCE = "repeat_dismiss_data_shared-preference";
        public static String REPEAT = "repeat";
        public static String DISMISS = "dismiss";
        public static String CLICKTODISMISS = "click to dismiss";
        public static String START_REPEATCOLOR = "start repeat color";
        public static String END_REPEATCOLOR = "end repeat color";
        public static String START_DISMISSCOLOR = "start dismiss color";
        public static String END_DISMISSCOLOR = "end dismiss color";
        public static String AUTOMATICCONTRAST = "automatic constrat";
        public static String START_REPEATTEXTCOLOR = "start text repeat color";
        public static String END_REPEATTEXTCOLOR = "end text repeat color";
        public static String START_DISMISSTEXTCOLOR = "start text dismiss color";
        public static String END_DISMISSTEXTCOLOR = "end text dismiss color";

        public Boolean repeat;
        public Boolean dismiss;
        public Boolean clickToDismiss;
        public Color[] repeatColor;
        public Color[] dismissColor;
        public boolean automaticContrast;
        public Color[] repeatTextColor;
        public Color[] dismissTextColor;

        public RepeatDismissData() {}
        public RepeatDismissData(Context context, Boolean repeat, Boolean dismiss, Boolean clickToDismiss) {
            this(   repeat,
                    dismiss,
                    clickToDismiss,
                    new Color[]{Color.valueOf(ContextCompat.getColor(context, R.color.snooze)), Color.valueOf(ContextCompat.getColor(context, R.color.snooze))},
                    new Color[]{Color.valueOf(ContextCompat.getColor(context, R.color.cancel)), Color.valueOf(ContextCompat.getColor(context, R.color.cancel))},
                    false,
                    new Color[]{Color.valueOf(0xffffffff), Color.valueOf(0xffffffff)},
                    new Color[]{Color.valueOf(0xffffffff), Color.valueOf(0xffffffff)}
            );
        }

        public RepeatDismissData(Boolean repeat, Boolean dismiss, Boolean clickToDismiss, Color[] repeatColor, Color[] dismissColor, boolean automaticContrast, Color[] repeatTextColor, Color[] dismissTextColor) {
            this.repeat = repeat;
            this.dismiss = dismiss;
            this.clickToDismiss = clickToDismiss;

            this.repeatColor = repeatColor;
            this.dismissColor = dismissColor;
            this.automaticContrast = automaticContrast;
            this.repeatTextColor = repeatTextColor;
            this.dismissTextColor = dismissTextColor;
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            RepeatDismissData data = (RepeatDismissData) object;
            if (!this.repeat.equals(data.repeat)) return false;
            if (!this.dismiss.equals(data.dismiss)) return false;
            if (!this.clickToDismiss.equals(data.clickToDismiss)) return false;
            if (!Arrays.equals(this.repeatColor, data.repeatColor)) return false;
            if (!Arrays.equals(this.dismissColor, data.dismissColor)) return false;
            if (this.automaticContrast != data.automaticContrast) return false;
            if (!Arrays.equals(this.repeatTextColor, data.repeatTextColor)) return false;
            if (!Arrays.equals(this.dismissTextColor, data.dismissTextColor)) return false;

            return true;
        }
    }
}
