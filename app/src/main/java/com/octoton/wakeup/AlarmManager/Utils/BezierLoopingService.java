package com.octoton.wakeup.AlarmManager.Utils;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

public abstract class BezierLoopingService {
    String msg = "Bezier Looper service : ";
    private Handler handler;
    private Runnable runnable;
    private final PathMeasure pathMeasure;
    private final int duration;
    private final float step;
    private float[] next_point;
    private float distance;
    private int delay;
    private float current_t;

    HandlerThread handlerThread;

    public BezierLoopingService(Path path, int duration, float step) {
        this.duration = duration;
        this.step = step;

        this.next_point = new float[2];
        this.pathMeasure = new PathMeasure(path, false);
        this.distance = 0;
    }

    public void onCreate(Looper looper) {
        handlerThread = new HandlerThread("MyHandlerThread");
        handlerThread.start();
        Looper serviceLooper = handlerThread.getLooper();
        handler = new Handler(serviceLooper);
    }

    public void onStart() {
        Log.d(msg, "The onStart() event");

        float length = pathMeasure.getLength();

        handler.postDelayed( runnable = new Runnable() {
            public void run() {
                action(next_point);

                // calculate next point
                current_t = next_point[0] * duration;
                distance += step * length;
                if (distance >= length) {
                    distance = length;
                }
                pathMeasure.getPosTan(distance, next_point, null);
                delay = (int) (next_point[0] * duration - current_t);

                //Log.d(msg, "Delay: " + delay + " Time: " + current_t + " " + debugInfo());

                if (delay > 0.0) {
                    handler.postDelayed(runnable, delay);
                }
            }
        }, delay);
    }

    public void onStop() {
        Log.d(msg, "The onStop() event");
        handler.removeCallbacks(runnable);
        handlerThread.quit();
    }

    abstract public void action(float[] next_point);

    public String debugInfo() {
        return "abstract bezier looping fragment with no action";
    }
}
