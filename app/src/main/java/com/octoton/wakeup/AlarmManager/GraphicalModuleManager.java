package com.octoton.wakeup.AlarmManager;

import static com.octoton.wakeup.AlarmReceiver.NOTIFICATION_ID;
import static com.octoton.wakeup.AlarmReceiver.SNOOZE_ALARM;
import static com.octoton.wakeup.AlarmReceiver.STOP_ALARM;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.Utils.AlarmListener;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Notification.InfoNotification;
import com.octoton.wakeup.R;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class GraphicalModuleManager extends AppCompatActivity implements AlarmListener {

    // Managing communication with AlarmService
    static private final String BROADCAST_LINK = "graphical broadcast link";

    static public void sendActiveStep(Context context, Step step, Step previousStep) {
        Intent intent = new Intent(BROADCAST_LINK);
        intent.putExtra("type", AlarmReceiver.GET_STEP);

        intent.putExtra("data", step);
        intent.putExtra("previousData", previousStep);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    static public void sendStopSignal(Context context) {
        Intent intent = new Intent(BROADCAST_LINK);
        intent.putExtra("type", AlarmReceiver.STOP_ALARM);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    static public void sendSnoozeSignal(Context context) {
        Intent intent = new Intent(BROADCAST_LINK);
        intent.putExtra("type", AlarmReceiver.SNOOZE_ALARM);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private boolean isInForeground = false;
    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            Log.d(msg, "onReceive");
            String type = intent.getStringExtra("type");

            if (type.equals(AlarmReceiver.GET_STEP) && isInForeground) {
                previousStep = intent.getParcelableExtra("previousData", Step.class);
                step = intent.getParcelableExtra("data", Step.class); // /!\ reference of step, not copy of step
                // In Android, when you use getParcelableExtra() to retrieve a Parcelable object from an Intent, it returns a reference to the original object, not a copy of the object. This behavior is by design and can be more efficient in terms of memory usage.

                loadFragment();
            } else if (type.equals(STOP_ALARM) || type.equals(SNOOZE_ALARM)) {
                stop();
            }
        }
    };

    private void stop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);

        finish();
        //finishAndRemoveTask(); // instead of finish() + android:excludeFromRecents="true" only for after API 21, below only flag FLAG_ACTIVITY_CLEAR_TOP will works
    }

    private void askForActiveStep() {
        Log.d(msg, AlarmReceiver.GET_STEP);
        Intent intentService = new Intent(this, AlarmService.class);
        intentService.putExtra(AlarmService.ACTION, AlarmReceiver.GET_STEP);
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        this.startForegroundService(intentService);
    }

    // Managing communication with fragment
    @Override
    public void onCancel() {
        Log.d(msg, "onCancel");
        Intent intentService = new Intent(this, AlarmService.class);
        intentService.putExtra(AlarmService.ID, id);
        intentService.putExtra(AlarmService.ACTION, STOP_ALARM);
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        this.startForegroundService(intentService);
    }

    @Override
    public void onRepeat() {
        Log.d(msg, "onRepeat");
        Intent intentService = new Intent(this, AlarmService.class);
        intentService.putExtra(AlarmService.ID, id);
        intentService.putExtra(AlarmService.STEP, step.number);
        intentService.putExtra(AlarmService.ACTION, SNOOZE_ALARM);
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        this.startForegroundService(intentService);
    }

    // Activity
    static String msg = "Alarm : ";
    boolean asAlreadyDoneOneStep;
    int id;
    List<Fragment> modules;
    Step step;
    Step previousStep;
    private RelativeLayout layout;

    SensorManager sensorManager;
    SensorEventListener sensorEventListener;
    ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
    boolean sensorListenerWasActivated;

    private void showActivity() {
        Log.d(msg, "show ativity");
        // This registers messageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(messageReceiver, new IntentFilter(BROADCAST_LINK));

        askForActiveStep();

        // Remove the listener to prevent multiple calls
        rootActivity.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    private void setOrientationAndShowActivity(int orientation) {
        rootActivity.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
        setRequestedOrientation(orientation);
        rootActivity.requestLayout(); // ensure GlobalLayoutListener is called even if set rotation do nothing (because orientation is already correct)
        rootActivity.invalidate();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(msg, "onCreate");
        super.onCreate(savedInstanceState);

        isInForeground = true;

        // full-screen activity
        Window window = getWindow();
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowCompat.setDecorFitsSystemWindows(window, false);
        WindowInsetsControllerCompat windowInsetsController =
                WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
        // Configure the behavior of the hidden system bars.
        windowInsetsController.setSystemBarsBehavior(
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        );
        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars());

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor gravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        if (gravity != null) {
            sensorEventListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    sensorListenerWasActivated = true;
                    sensorManager.unregisterListener(this);

                    int gravityAxis = 0;
                    if (Math.abs(sensorEvent.values[1]) > Math.abs(sensorEvent.values[gravityAxis]))
                        gravityAxis = 1;

                    int orientation;
                    if (gravityAxis == 0) { // x
                        if (sensorEvent.values[gravityAxis] >= 0) { orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE; }
                        else { orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE; }
                    } else { // y
                        if (sensorEvent.values[gravityAxis] >= 0) { orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT; }
                        else { orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT; }
                    }

                    setOrientationAndShowActivity(orientation);
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };
        } /*else {

        }*/
        setContentView(R.layout.activity_alarm);

        //set flags so activity is showed when phone is off (on lock screen)
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
        // For newer than Android Oreo: call setShowWhenLocked, setTurnScreenOn
        setShowWhenLocked(true);
        setTurnScreenOn(true);

        // If you want to display the keyguard to prompt the user to unlock the phone:
        /*KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        keyguardManager.requestDismissKeyguard(this, null);*/
      /*} else {
         // For older versions, do it as you did before.
         getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
      }*/

        //if (savedInstanceState == null) {
        id = getIntent().getIntExtra(NOTIFICATION_ID, 0);
        step = null;
        previousStep = null;

        rootActivity = (RelativeLayout) findViewById(R.id.root);

        rootActivity.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();

                if (!fragments.isEmpty()) {
                    for (Fragment fragment : fragments) {
                        ((Module)fragment).activityRegisteredCLick();
                    }
                }
            }
        });

        layout = new RelativeLayout(this);
        layout.setId(View.generateViewId());
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        rootActivity.addView(layout);

        asAlreadyDoneOneStep = false;
        sensorListenerWasActivated = false;
        onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                showActivity();
            }
        };
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() { // In case gravity sensor doesn't trigger
            @Override
            public void run() {
                if (isInForeground && !sensorListenerWasActivated) {
                    sensorManager.unregisterListener(sensorEventListener);

                    int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    SharedPreferences sharedPreferences = androidx.preference.PreferenceManager.getDefaultSharedPreferences(GraphicalModuleManager.this);
                    String name = sharedPreferences.getString("default_orientation", "NONE");
                    if (!name.equals("NONE")) {
                        switch (name) {
                            //case "SCREEN_ORIENTATION_PORTRAIT": orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT; break;
                            case "SCREEN_ORIENTATION_REVERSE_PORTRAIT": orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT; break;
                            case "SCREEN_ORIENTATION_LANDSCAPE": orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE; break;
                            case "SCREEN_ORIENTATION_REVERSE_LANDSCAPE": orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE; break;
                        }
                    } else {
                        InfoNotification.showNotificationOrientationUndetected(GraphicalModuleManager.this);
                    }
                    setOrientationAndShowActivity(orientation);
                }
            }
        }, 3000); // TODO: landscape doesn't not work in debug mode (at least) if delay is to little (ex.: 500ms), in that case orientation is done to quickly and width is not yet defined
        sensorManager.registerListener(sensorEventListener, gravity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    RelativeLayout rootActivity;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "onStart");

        isInForeground = true;
    }

    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "onResume");

        isInForeground = true;
    }

    private void loadFragment() {
        Log.d(msg, "loadFragment");
        FragmentManager fragmentManager = getSupportFragmentManager();

        Activity context = this;
        AlarmDatabase db = AlarmDatabase.getDatabase(context);
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() ->
                {   AlarmModel alarmModel = db.alarmModel().getItemByIdSynchronous(id); //TODO: only do this once, instead of every next step ?
                    ModuleManager.graphicalCommit(
                        context,
                        fragmentManager,
                        ModuleManager.loadGraphicalModule(context, fragmentManager, db.moduleModel(), alarmModel, layout.getId(), step, previousStep, !asAlreadyDoneOneStep),
                        step);
                    asAlreadyDoneOneStep = true;
                }
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "onPause");

        isInForeground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "onStop");

        Log.d(msg, "unregisterListener");
        sensorManager.unregisterListener(sensorEventListener);

        isInForeground = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "onDestroy");

        isInForeground = false;
    }
}
