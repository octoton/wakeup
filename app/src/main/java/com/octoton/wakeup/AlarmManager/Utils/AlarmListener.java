package com.octoton.wakeup.AlarmManager.Utils;

public interface AlarmListener {
    public void onCancel();
    public void onRepeat();
}
