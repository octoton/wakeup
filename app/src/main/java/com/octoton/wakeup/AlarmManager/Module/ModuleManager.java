package com.octoton.wakeup.AlarmManager.Module;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.octoton.wakeup.AlarmManager.AlarmService;
import com.octoton.wakeup.AlarmManager.ClockFragment;
import com.octoton.wakeup.AlarmManager.ColorGradientFragment;
import com.octoton.wakeup.AlarmManager.FlashService;
import com.octoton.wakeup.AlarmManager.LightFragment;
import com.octoton.wakeup.AlarmManager.RepeatDismissFragment;
import com.octoton.wakeup.AlarmManager.SoundService;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.AlarmManager.VibratorService;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmModelDao;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.Database.ModuleModelDao;
import com.octoton.wakeup.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModuleManager {
    public static void graphicalCommit(Activity context, FragmentManager fragmentManager, List<Fragment> fragments, Step step) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (Fragment fragment : fragments) {
                    ((Module)fragment).start(step.currentDuration);
                    Integer id = ((Module)fragment).getParentId();
                    FragmentTransaction transaction = fragmentManager.beginTransaction()
                            .setReorderingAllowed(true);
                    if (id == null) {
                        transaction.add(fragment, ((Module)fragment).getData().name);
                    } else {
                        transaction.add(id, fragment, ((Module)fragment).getData().name);
                    }
                    transaction.commitAllowingStateLoss(); //commitAllowingStateLoss(); --     java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
                }
            }
        });
    }

    public static void moduleCommit(List<Module> services, int position) {
        for (Module service : services) {
            service.start(position);
        }
    }

    public static void write(Context context, String sharedPreference, MODULE_TAG moduleTag, Module.Data data) {
        if (!sharedPreference.isEmpty()) {
            SharedPreferences sharedPref = context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();

            MODULE_TAG.write(moduleTag, editor, data);

            editor.apply();
        }
    }

    public static void remove(Context context, String sharedPreference) {
        if (!sharedPreference.isEmpty()) {
            SharedPreferences sharedPref = context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE);
            sharedPref.edit().clear().commit();

            File dir = new File(context.getApplicationInfo().dataDir, "shared_prefs");
            (new File(dir, sharedPreference + ".xml")).delete();
        }
    }

    static public List<Fragment> loadGraphicalModule(Activity context, FragmentManager fragmentManager, ModuleModelDao moduleModelDao, AlarmModel alarmModel, int layout_id, Step step, Step previousStep, boolean launchFromCold) {
        List<ModuleModel> modulesInModel = moduleModelDao.getAllGraphicalModuleItemsOfProfileSynchronous(alarmModel.getProfileId(), step.number);
        List<MODULE_TAG> previousModulesTag = (previousStep != null) ? new ArrayList<>(previousStep.modulesTag) : null;
        List<Fragment> list = new ArrayList<>();
        for (ModuleModel moduleModel: modulesInModel) {
            String sharedPref = moduleModel.getSharedPreferenceName();
            Fragment module;

            if (launchFromCold || (previousModulesTag != null && !previousModulesTag.contains(moduleModel.getModuleTag()))) {
                module = MODULE_TAG.createGraphicalModule(moduleModel.getModuleTag(), layout_id);

                ((Module)module).load(context, context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE), alarmModel, moduleModel);
                list.add(module);
            } else {
                module = MODULE_TAG.findGraphicalModule(moduleModel.getModuleTag(), fragmentManager);

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO: to investigate, but it seems this can be called when alarm is in background at next step transition and thus it crashed because module is null
                        ((Module)module).reload(context, context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE), alarmModel, moduleModel, step);
                    }
                });
            }
            if (previousModulesTag != null) previousModulesTag.remove(moduleModel.getModuleTag());
        }

        if (!launchFromCold && previousModulesTag != null) {
            Log.d("graphic module", "removal: " + previousModulesTag);
            for (MODULE_TAG deletedModuleTag : previousModulesTag) {
                if (deletedModuleTag.isGraphical()) {
                    Fragment module = MODULE_TAG.findGraphicalModule(deletedModuleTag, fragmentManager);

                    FragmentTransaction transaction = fragmentManager.beginTransaction().setReorderingAllowed(true);
                    transaction.remove(module).commit(); //commitAllowingStateLoss();
                }
            }
        }

        return list;
    }

    static public List<Module> loadServiceModule(Context context, List<Module> moduleServices, ModuleModelDao moduleModelDao, AlarmModel alarmModel, Step step, Step previousStep, boolean launchFromCold) {
        List<ModuleModel> modulesInModel = moduleModelDao.getAllServiceModuleItemsOfProfileSynchronous(alarmModel.getProfileId(), step.number);
        List<MODULE_TAG> previousModulesTag = (previousStep != null) ? new ArrayList<>(previousStep.modulesTag) : null;
        List<Module> list = new ArrayList<>();
        for (ModuleModel moduleModel: modulesInModel) {
            String sharedPref = moduleModel.getSharedPreferenceName();
            Module module;

            if (launchFromCold || (previousModulesTag != null && !previousModulesTag.contains(moduleModel.getModuleTag()))) {
                module = MODULE_TAG.createServiceModule(moduleModel.getModuleTag());

                Log.d("service module", "load: "+moduleModel.getModuleTag());
                module.load(context, context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE), alarmModel, moduleModel);
                list.add(module);
            } else {
                module = MODULE_TAG.findServiceModule(moduleModel.getModuleTag(), moduleServices);

                Log.d("service module", "reload: "+moduleModel.getModuleTag());
                module.reload(context, context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE), alarmModel, moduleModel, step);

                list.add(module);
            }
            if (previousModulesTag != null) previousModulesTag.remove(moduleModel.getModuleTag());
        }


        if (!launchFromCold && previousModulesTag != null) {
            Log.d("service module", "removal: " + previousModulesTag);
            for (MODULE_TAG deletedModuleTag : previousModulesTag) {
                if (!deletedModuleTag.isGraphical()) {
                    Module module = MODULE_TAG.findServiceModule(deletedModuleTag, moduleServices);

                    if (module != null) {
                        module.stop();
                        moduleServices.remove(module);
                    }
                }
            }
        }
        return list;
    }

    public enum MODULE_TAG {
        SOUND_SERVICE(0, false),
        VIBRATOR_SERVICE(1, false),
        FLASH_SERVICE(2, false),

        REPEAT_DISMISS_SERVICE(0, true),
        LIGHT_SERVICE(1, true),
        COLOR_GRADIENT_SERVICE(2, true),
        CLOCK_SERVICE(3, true);

        public final static int GRAPHICAL_OFFSET = 100;

        private final int tag;
        MODULE_TAG(int tag, boolean isGraphical) {
            if (isGraphical) {
                this.tag = tag + GRAPHICAL_OFFSET;
            } else {
                this.tag = tag;
            }
        }

        public static void write(MODULE_TAG moduleTag, SharedPreferences.Editor editor, Module.Data data) {
            switch (moduleTag) {
                case SOUND_SERVICE:
                    SoundService.write(editor, data);
                    break;
                case VIBRATOR_SERVICE:
                    VibratorService.write(editor, data);
                    break;
                case FLASH_SERVICE:
                    FlashService.write(editor, data);
                    break;
                case REPEAT_DISMISS_SERVICE:
                    RepeatDismissFragment.write(editor, data);
                    break;
                case LIGHT_SERVICE:
                    LightFragment.write(editor, data);
                    break;
                case COLOR_GRADIENT_SERVICE:
                    ColorGradientFragment.write(editor, data);
                    break;
                case CLOCK_SERVICE:
                    ClockFragment.write(editor, data);
                    break;
                default:
            }
        }

        public static Fragment findGraphicalModule(MODULE_TAG moduleTag, FragmentManager fragmentManager) {
            switch (moduleTag) {
                case REPEAT_DISMISS_SERVICE:
                    return (RepeatDismissFragment)fragmentManager.findFragmentByTag(RepeatDismissFragment.RepeatDismissData.SHARED_PREFERENCE);
                case LIGHT_SERVICE:
                    return (LightFragment)fragmentManager.findFragmentByTag(LightFragment.LightData.SHARED_PREFERENCE);
                case COLOR_GRADIENT_SERVICE:
                    return (ColorGradientFragment)fragmentManager.findFragmentByTag(ColorGradientFragment.ColorGradientData.SHARED_PREFERENCE);
                case CLOCK_SERVICE:
                    return (ClockFragment)fragmentManager.findFragmentByTag(ClockFragment.ClockData.SHARED_PREFERENCE);
                default:
                    return null;
            }
        }

        public static Fragment createGraphicalModule(MODULE_TAG moduleTag, int layout_id) {
            switch (moduleTag) {
                case REPEAT_DISMISS_SERVICE:
                    return new RepeatDismissFragment(layout_id);
                case LIGHT_SERVICE:
                    return new LightFragment();
                case COLOR_GRADIENT_SERVICE:
                    return new ColorGradientFragment(layout_id);
                case CLOCK_SERVICE:
                    return new ClockFragment(R.id.root);
                default:
                    return null;
            }
        }

        public static Module findServiceModule(MODULE_TAG moduleTag, List<Module> moduleServices) {
            String name = "";
            Log.d("module: ", "tag: "+moduleTag);
            switch (moduleTag) {
                case SOUND_SERVICE:
                    name = SoundService.SoundData.SHARED_PREFERENCE;
                    break;
                case VIBRATOR_SERVICE:
                    name = VibratorService.VibratorData.SHARED_PREFERENCE;
                    break;
                case FLASH_SERVICE:
                    name = FlashService.FlashData.SHARED_PREFERENCE;
                    break;
                default:
            }
            for (Module module: moduleServices) {
                Log.d("module: ", "name: "+module.getData().name);
                if (name.equals(module.getData().name)) {
                    Log.d("module: ", "founded");
                    return module;
                }
            }
            Log.d("module: ", "nothing");

            return null;
        }

        public static Module createServiceModule(MODULE_TAG moduleTag) {
            switch (moduleTag) {
                case SOUND_SERVICE:
                    return new SoundService();
                case VIBRATOR_SERVICE:
                    return new VibratorService();
                case FLASH_SERVICE:
                    return new FlashService();
                default:
                    return null;
            }
        }

        public int getTag() { return tag; }
        public boolean isGraphical() { return (tag >= GRAPHICAL_OFFSET); }

        public static Module.Data getData(MODULE_TAG moduleTag, Context context, ModuleModel moduleModel) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(moduleModel.getSharedPreferenceName(), Context.MODE_PRIVATE);

            switch (moduleTag) {
                case REPEAT_DISMISS_SERVICE:
                    return RepeatDismissFragment.loadData(context, sharedPreferences, moduleModel);
                case LIGHT_SERVICE:
                    return LightFragment.loadData(context, sharedPreferences, moduleModel);
                case COLOR_GRADIENT_SERVICE:
                    return ColorGradientFragment.loadData(context, sharedPreferences, moduleModel);
                case CLOCK_SERVICE:
                    return ClockFragment.loadData(context, sharedPreferences, moduleModel);
                case SOUND_SERVICE:
                    return SoundService.loadData(context, sharedPreferences, moduleModel);
                case VIBRATOR_SERVICE:
                    return VibratorService.loadData(context, sharedPreferences, moduleModel);
                case FLASH_SERVICE:
                    return FlashService.loadData(context, sharedPreferences, moduleModel);
                default:
                    return null;
            }
        }

        public static void getAccessRight(ModuleModel moduleModel, Activity activity) {
            switch (moduleModel.getModuleTag()) {
                case SOUND_SERVICE:
                    SoundService.askForPermission(activity, !((SoundService.SoundData)moduleModel.getData()).songIsRaw(activity));
                    break;
                case LIGHT_SERVICE:
                    LightFragment.askForPermission(activity);
                    break;
                case REPEAT_DISMISS_SERVICE:
                case COLOR_GRADIENT_SERVICE:
                case CLOCK_SERVICE:
                case VIBRATOR_SERVICE:
                case FLASH_SERVICE:
                default:
                    break;
            }
        }
    }
}
