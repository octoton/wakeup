package com.octoton.wakeup.AlarmManager.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class RandomUtils {

    // Found a random int in [0, bound] that is not in forbiddenInteger
    public static int nextIntBoundWithForbiddenNumber(int bound, ArrayList<Integer> forbiddenInteger) {
        Set<Integer> forbiddenIntegerSet = new TreeSet<>(forbiddenInteger);

        int reduceBound = bound - forbiddenIntegerSet.size();
        if (reduceBound > 0) {
            // get a random position in reduce interval
            int random = new Random().nextInt(reduceBound);

            // if random is forbidden, add 1
            int previousForbiddenNumber = -1;
            for (int forbiddenNumber : forbiddenIntegerSet) {
                if (forbiddenNumber != previousForbiddenNumber && random >= forbiddenNumber) {
                    random++;
                } else if (random < forbiddenNumber) {
                    break;
                }
                previousForbiddenNumber = forbiddenNumber;
            }
            return random;
        } else if (reduceBound == 0) {
            return 0;
        }
        return -1;
    }

}
