package com.octoton.wakeup.AlarmManager.Animator;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

public class AnimatorManager {
    String msg = "Animator manager : ";

    private boolean isRunning;
    private boolean isFinished;
    private Runnable runnable;
    private final Handler serviceHandler;


    private final List<Animator> animationToAdd;
    private final List<Animator> animationList;

    static AnimatorManager serviceAnimator;
    static public AnimatorManager GetServiceAnimatorManager(Handler serviceHandler) {
        if (serviceAnimator == null || serviceAnimator.isFinished()) {
            if (serviceHandler == null) {
                throw new RuntimeException("Service handler is null");
            }
            serviceAnimator = new AnimatorManager(serviceHandler);
        }
        return serviceAnimator;
    }

    public AnimatorManager(Handler serviceHandler) {
        isRunning = false;
        isFinished = false;
        animationToAdd = new ArrayList<>();
        animationList = new ArrayList<>();

        this.serviceHandler = serviceHandler;
    }

    public void addAnimation(Animator animation) {
        animationToAdd.add(animation);
    }

    public boolean isStared() {
        return isRunning;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void onStart() {
        isRunning = true;

        long delay = 1000 / 24; // 24fps in ms
        serviceHandler.postDelayed(runnable = new Runnable() {
            public void run() {
                if (isRunning) {
                    long beforeTime = System.currentTimeMillis();

                    for (int i = animationList.size()-1; i>=0; i--) {
                        Animator animation = animationList.get(i);
                        animation.animateBaseOnTime(beforeTime);

                        if (animation.isFinish()) {
                            animationList.remove(i);
                        }
                    }

                    for (int i = animationToAdd.size()-1; i>=0; i--) {
                        Animator animation = animationToAdd.get(i);

                        if (!animationList.contains(animation)) {
                            animationList.add(animation);
                            animationToAdd.remove(i);
                        }
                    }

                    long afterTime = System.currentTimeMillis();
                    long currentDelay = delay - (afterTime - beforeTime);
                    if (currentDelay < 0) { currentDelay = 0; }

                    serviceHandler.postDelayed(runnable, currentDelay);
                }
            }
        }, delay);
    }

    public void onStop() {
        isRunning = false;
        isFinished = true;

        serviceHandler.removeCallbacks(runnable);
    }
}
