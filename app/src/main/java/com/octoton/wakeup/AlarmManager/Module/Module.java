package com.octoton.wakeup.AlarmManager.Module;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Path;

import com.octoton.wakeup.AlarmManager.AlarmService;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;

public interface Module {

    Data getData();
    void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel);
    void start(int position);
    void stop();
    void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step);
    Integer getParentId();

    void activityRegisteredCLick();

    class Data {
        public String name;
        public int duration;
        public int step_number;
        public Path path;
    }
}
