package com.octoton.wakeup.AlarmManager.Utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;

import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.DataModel.Interface.AlarmModelInterface;
import com.octoton.wakeup.DataModel.Interface.ProfileModelInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Step implements Parcelable {
    static String msg = "Step";
    public final int profileId;
    public final int number;
    public final int maxNumber;
    public final int maxDuration;
    public final int day;

    public int currentDuration;
    public List<ModuleManager.MODULE_TAG> modulesTag;

    public boolean isLastStep() {
        return this.number < this.maxNumber;
    }

    public Step(ProfileModelInterface profileModel, AlarmModelInterface alarmModel, int id, int number, int day) {
        this.profileId = alarmModel.getProfileIdOfAlarm(id);
        this.maxNumber = profileModel.getMaxStepNumberInModuleItemsOfProfile(this.profileId);
        this.number = Math.min(this.maxNumber, number);
        this.day = day;

        this.currentDuration = 0;

        this.maxDuration = profileModel.getMaxDurationInModuleItemsOfProfileForStep(this.profileId, this.number);
        this.modulesTag = profileModel.getAllModuleItemsOfProfile(this.profileId, this.number);
    }

    protected Step(Parcel in) {
        this.profileId = in.readInt();
        this.number = in.readInt();
        this.currentDuration = in.readInt();
        this.maxNumber = in.readInt();
        this.maxDuration = in.readInt();
        this.day = in.readInt();

        this.modulesTag = new ArrayList<>();
        List<String> modulesTagString = new ArrayList<>();
        in.readTypedList(modulesTagString, null);
        for (String moduleTagString : modulesTagString) {
            this.modulesTag.add(ModuleManager.MODULE_TAG.valueOf(moduleTagString));
        }
        Log.d(msg, "Alarm service: Step");
    }

    public static final Creator<Step> CREATOR = new Creator<Step>() {
        @Override
        public Step createFromParcel(Parcel in) {
            return new Step(in);
        }

        @Override
        public Step[] newArray(int size) {
            return new Step[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(this.profileId);
        dest.writeInt(this.number);
        dest.writeInt(this.currentDuration);
        dest.writeInt(this.maxNumber);
        dest.writeInt(this.maxDuration);
        dest.writeInt(this.day);

        List<String> modulesTagString = new ArrayList<>();
        for (ModuleManager.MODULE_TAG moduleTag : this.modulesTag) {
            modulesTagString.add(moduleTag.name());
        }
        dest.writeList(modulesTagString);
    }
}