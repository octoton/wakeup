package com.octoton.wakeup.AlarmManager.Utils;

import android.graphics.Color;

import androidx.core.graphics.ColorUtils;

public class ColorEvaluator implements android.animation.TypeEvaluator {
    @Override
    public Object evaluate(float v, Object o, Object t1) {
        if (o == null || t1 == null) return null;
        if (o.getClass() != Color.class || t1.getClass() != Color.class) return null;

        Color c0 = (Color)o;
        Color c1 = (Color)t1;

        return hslGradient(c0, c1, v);
    }

    private Color linearGradient(Color c0, Color c1, float ratio) {
        return Color.valueOf(
                (c1.red() - c0.red()) * ratio + c0.red(),
                (c1.green() - c0.green()) * ratio + c0.green(),
                (c1.blue() - c0.blue()) * ratio + c0.blue(),
                (c1.alpha() - c0.alpha()) * ratio + c0.alpha());
    }

    private Color hsvGradient(Color c0, Color c1, float ratio) {
        float[] hsv0 = new float[3];
        Color.colorToHSV(stripAlpha(c0.toArgb()), hsv0);
        float[] hsv1 = new float[3];
        Color.colorToHSV(stripAlpha(c1.toArgb()), hsv1);

        float[] hsv  = new float[3];
        hsv[0] = (hsv1[0] - hsv0[0]) * ratio + hsv0[0];
        hsv[1] = (hsv1[1] - hsv0[1]) * ratio + hsv0[1];
        hsv[2] = (hsv1[2] - hsv0[2]) * ratio + hsv0[2];

        Color result = Color.valueOf(Color.HSVToColor(hsv));
        return Color.valueOf(
                result.red(),
                result.green(),
                result.blue(),
                (c1.alpha() - c0.alpha()) * ratio + c0.alpha());
    }

    private Color hslGradient(Color c0, Color c1, float ratio) {
        float[] hsl0 = new float[3];
        ColorUtils.colorToHSL(stripAlpha(c0.toArgb()), hsl0);
        float[] hsl1 = new float[3];
        ColorUtils.colorToHSL(stripAlpha(c1.toArgb()), hsl1);

        float[] hsl  = new float[3];
        hsl[0] = (hsl1[0] - hsl0[0]) * ratio + hsl0[0];
        hsl[1] = (hsl1[1] - hsl0[1]) * ratio + hsl0[1];
        hsl[2] = (hsl1[2] - hsl0[2]) * ratio + hsl0[2];

        Color result = Color.valueOf(ColorUtils.HSLToColor(hsl));
        return Color.valueOf(
                result.red(),
                result.green(),
                result.blue(),
                (c1.alpha() - c0.alpha()) * ratio + c0.alpha());
    }

    public static int stripAlpha(int color){
        return Color.rgb(Color.red(color), Color.green(color), Color.blue(color));
    }
}
