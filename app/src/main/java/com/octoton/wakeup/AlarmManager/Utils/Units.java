package com.octoton.wakeup.AlarmManager.Utils;

public class Units {
    public static int ms = 1;
    public static int s = 1000 * ms;
    public static int min = 60 * s;
}
