package com.octoton.wakeup.AlarmManager;

import android.animation.IntEvaluator;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.octoton.wakeup.AlarmManager.Animator.Animator;
import com.octoton.wakeup.AlarmManager.Animator.AnimatorManager;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.AlarmReceiver;
import com.octoton.wakeup.DataModel.Synchronous.AlarmModelSynchronous;
import com.octoton.wakeup.DataModel.Synchronous.ProfileModelSynchronous;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Notification.AlarmNotification;

public class AlarmService extends Service {

    public static String ACTION = "alarmService.action";
    public static String STEP = "alarmService.step";
    public static String DAY = "alarmService.day";
    public static String ID = "alarmService.id";

    AlarmModelSynchronous alarmModel;
    ProfileModelSynchronous profileModel;

    int id;
    int notificationId;

    Step step;
    Step previousStep;

    Looper serviceLooper;
    Handler serviceHandler;
    HandlerThread handlerThread;

    Animator durationAnimator;
    AnimatorManager animatorManager;
    private ServiceModuleManager serviceModuleManager;

    private State status;
    private enum State {
        INIT,
        START,
        RUN,
        STOP,
        SNOOZE
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.handlerThread = new HandlerThread("AlarmServiceHandlerThread");
        this.handlerThread.start();

        this.serviceLooper = this.handlerThread.getLooper();
        this.serviceHandler = new Handler(this.serviceLooper, new MessageHandler(this));

        this.status = State.INIT;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);

        //TODO: when waiting too much to click on notification, intent become null "Caused by: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String android.content.Intent.getStringExtra(java.lang.String)' on a null object reference"
        String action = intent.getStringExtra(ACTION);
        if ((AlarmReceiver.GET_STEP).equals(action)) {
            GraphicalModuleManager.sendActiveStep(this, this.step, this.previousStep);
        } else if ((AlarmReceiver.SNOOZE_ALARM).equals(action)) {
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = startId;
            msg.arg2 = intent.getIntExtra(STEP, 0);
            msg.what = MessageHandler.SNOOZE;

            this.serviceHandler.sendMessage(msg);
        } else if ((AlarmReceiver.STOP_ALARM).equals(action)) {
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = startId;
            msg.what = MessageHandler.STOP;

            serviceHandler.sendMessage(msg);
        } else if ((AlarmReceiver.START_ALARM).equals(action)) {
            this.status = State.START;
            int stepNumber = intent.getIntExtra(STEP, 0);
            int day = intent.getIntExtra(DAY, 0);
            this.id = intent.getIntExtra(ID, 0);

            this.alarmModel = new AlarmModelSynchronous(this);
            this.profileModel = new ProfileModelSynchronous(this);

            this.notificationId = (int)System.currentTimeMillis()%10000;
            startForeground(this.notificationId, AlarmNotification.createNotification(this, this.id, day));

            this.animatorManager = AnimatorManager.GetServiceAnimatorManager(serviceHandler);
            this.serviceModuleManager = new ServiceModuleManager(AlarmDatabase.getDatabase(this), id);

            serviceHandler.post(() -> runner(stepNumber, day));
        }

        return START_NOT_STICKY;
    }

    private void runner(int stepNumber, int day) {
        this.status = State.RUN;
        this.alarmModel.setNextAlarmIfPossibleFor(this.id);

        this.step = new Step(this.profileModel, this.alarmModel, this.id, stepNumber, day);

        this.durationAnimator = Animator.ofObject(new IntEvaluator(), 0, this.step.maxDuration);
        this.durationAnimator.setInterpolator(new LinearInterpolator());
        this.durationAnimator.setDuration(this.step.maxDuration);
        this.durationAnimator.addUpdateListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) { }

            @Override
            public void onAnimationUpdate(Animator animator) { step.currentDuration = (int) animator.getAnimatedValue(); }

            @Override
            public void onAnimationEnd(Animator animator) { startNextStep(); }
        });

        this.durationAnimator.start(this.animatorManager);
        this.serviceModuleManager.sendActiveStep(this, this.step, this.previousStep);
    }

    private void startNextStep() {
        if (this.status == State.RUN && this.step.isLastStep()) {
            this.previousStep = step;
            this.step = new Step(this.profileModel, this.alarmModel, this.id, this.step.number + 1, this.step.day);
            
            this.durationAnimator.cancel();
            this.durationAnimator.setObjectValues(0, this.step.maxDuration);
            this.durationAnimator.setDuration(this.step.maxDuration);
            this.durationAnimator.start(this.animatorManager);

            serviceModuleManager.sendActiveStep(this, this.step, this.previousStep);
            GraphicalModuleManager.sendActiveStep(this, this.step, this.previousStep);
        }
    }

    private void snooze(int stepNumber) {
        this.status = State.SNOOZE;
        this.serviceModuleManager.stop();
        this.durationAnimator.cancel();
        GraphicalModuleManager.sendSnoozeSignal(this);
        previousStep = null;

        alarmModel.snooze(this.id, stepNumber + 1, this.step.day, this.step.maxDuration, this.notificationId);
    }

    private void stop() {
        this.status = State.STOP;
        this.serviceModuleManager.stop();
        this.durationAnimator.cancel();
        GraphicalModuleManager.sendStopSignal(this);
        previousStep = null;

        alarmModel.stop(this.id, this.step.number, this.step.day, this.notificationId);

        this.animatorManager.onStop();

        this.serviceHandler.removeCallbacksAndMessages(null);
        this.handlerThread.quit();

        stopSelf();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static class MessageHandler implements Handler.Callback {
        public static int STOP = 1;
        public static int SNOOZE = 2;

        AlarmService alarmServiceManager;

        private MessageHandler(AlarmService alarmServiceManager) {
            this.alarmServiceManager = alarmServiceManager;
        }

        @Override
        public boolean handleMessage(@NonNull Message message) {
            if (message.what == STOP) {
                this.alarmServiceManager.stop();
            } else if (message.what == SNOOZE) {
                this.alarmServiceManager.snooze(message.arg2);
            }

            return false;
        }
    }
}