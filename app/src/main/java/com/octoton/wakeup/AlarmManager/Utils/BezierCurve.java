package com.octoton.wakeup.AlarmManager.Utils;

import android.graphics.Path;
import android.util.Log;

import androidx.annotation.NonNull;

import com.octoton.wakeup.Database.ModuleModel;

public class BezierCurve {
    private float x0;
    private float y0;
    private float x1;
    private float y1;

    public BezierCurve(float x0, float y0, float x1, float y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }

    public Path getPath() {
        Path path = new Path();

        path.moveTo(0, 0);
        path.cubicTo(x0, y0, x1+1.0f, y1+1.0f, 1, 1);

        return path;
    }

    @NonNull
    @Override
    public String toString() {
        String value = "";

        value += ""+this.x0+", "+this.y0+", "+this.x1+", "+this.y1;

        return value;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (getClass() != object.getClass()) return false;

        BezierCurve curve = (BezierCurve) object;

        float epsilon = 0.00000001f;
        if (Math.abs(this.x0 - curve.x0) >= epsilon) return false;
        if (Math.abs(this.y0 - curve.y0) >= epsilon) return false;
        if (Math.abs(this.x1 - curve.x1) >= epsilon) return false;
        if (Math.abs(this.y1 - curve.y1) >= epsilon) return false;

        return true;
    }

    /* TODO: not possible to hash with epsilon
    @Override
    public int hashCode() {
        int hash = 3;

        hash = 89 * hash + (this.moduleTag == null ? 0 :this.moduleTag.hashCode());
        hash = 89 * hash + this.stepNumber;
        hash = 89 * hash + this.duration;
        hash = 89 * hash + (this.activationCurve == null ? 0 :this.activationCurve.hashCode());
        hash = 89 * hash + (this.sharedPreferenceNameCommonPart == null ? 0 :this.sharedPreferenceNameCommonPart.hashCode());

        return hash;
    }*/

    public float getX0() { return x0; }
    public float getY0() { return y0; }
    public float getX1() { return x1; }
    public float getY1() { return y1; }
}
