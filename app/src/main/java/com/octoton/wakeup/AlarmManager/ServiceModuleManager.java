package com.octoton.wakeup.AlarmManager;

import android.content.Context;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Module.ModuleManager;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;

import java.util.ArrayList;
import java.util.List;

public class ServiceModuleManager {
    private List<Module> moduleServices;
    private final AlarmDatabase db;
    private final int id;

    private boolean asAlreadyDoneOneStep;

    public ServiceModuleManager(AlarmDatabase db, int id) {
        this.db = db;
        this.id = id;
        moduleServices = new ArrayList<>();
        asAlreadyDoneOneStep = false;
    }

    public void sendActiveStep(Context context, Step step, Step previousStep) {
        AlarmModel alarmModel = db.alarmModel().getItemByIdSynchronous(id);

        moduleServices = ModuleManager.loadServiceModule(context, moduleServices, db.moduleModel(), alarmModel, step, previousStep, !asAlreadyDoneOneStep);
        ModuleManager.moduleCommit(moduleServices,0);
        asAlreadyDoneOneStep = true;
    }

    public void stop() {
        asAlreadyDoneOneStep = false;
        for (Module moduleService : moduleServices) {
            moduleService.stop();
        }
    }
}
