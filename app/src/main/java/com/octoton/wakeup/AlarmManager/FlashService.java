package com.octoton.wakeup.AlarmManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.util.Log;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;

public class FlashService implements Module {
    static String msg = "Flash service : ";

    FlashData data;

    CameraManager cameraManager;

    @Override
    public Integer getParentId() {
        return null;
    }

    @Override
    public void start(int position) {
        if (cameraManager == null) { return; }
        try {
            for (String id: cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(id);
                boolean hasFlash = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);

                if (hasFlash) {
                    cameraManager.setTorchMode(id, true);
                }
            }
            //cameraManager.turnOnTorchWithStrengthLevel(cameraManager.getCameraIdList()[0], 1);
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() {
        if (cameraManager == null) { return; }
        try {
            for (String id: cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(id);
                boolean hasFlash = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);

                if (hasFlash) {
                    cameraManager.setTorchMode(id, false);
                }
            }
            //cameraManager.turnOnTorchWithStrengthLevel(cameraManager.getCameraIdList()[0], 1);
        } catch (CameraAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step) {
        load(context, sharedPreferences, alarmModel, moduleModel);
    }

    public static FlashData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        FlashData data = new FlashData();

        data.name = FlashData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);

        boolean hasFlash = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (hasFlash) {
            cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        } else {
            cameraManager = null;
        }
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
    }

    @Override
    public void activityRegisteredCLick() {

    }

    @Override
    public Data getData() {
        return data;
    }


    public static class FlashData extends Module.Data {
        public static String SHARED_PREFERENCE = "flash_data_shared-preference";

        public FlashData() {
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            return true;
        }
    }
}