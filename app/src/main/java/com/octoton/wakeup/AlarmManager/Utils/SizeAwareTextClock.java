package com.octoton.wakeup.AlarmManager.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextClock;

import com.octoton.wakeup.R;

public class SizeAwareTextClock extends TextClock {

    private OnClockSizeChangedListener onClockSizeChangedListener;
    private float lastClockSize;

    public SizeAwareTextClock(Context context) {
        super(context);
        this.lastClockSize = getTextSize();
    }

    public SizeAwareTextClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.lastClockSize = getTextSize();
    }

    public SizeAwareTextClock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastClockSize = getTextSize();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.lastClockSize != getTextSize()) {
            this.lastClockSize = getTextSize();
            if (this.onClockSizeChangedListener != null) {
                this.onClockSizeChangedListener.onClockSizeChanged(this, this.lastClockSize);
            }
        }
    }

    public void setOnClockSizeChangedListener(OnClockSizeChangedListener onClockSizeChangedListener) {
        this.onClockSizeChangedListener = onClockSizeChangedListener;
    }

    public interface OnClockSizeChangedListener {
        void onClockSizeChanged(SizeAwareTextClock view, float textSize);
    }
}
