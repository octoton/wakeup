package com.octoton.wakeup.AlarmManager.Animator;

import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;

public class Animator {

    private TypeEvaluator evaluator;
    private Object startValue;
    private Object endValue;
    private TimeInterpolator interpolator;
    private long duration;
    private AnimatorListener updateListener;

    private long lastAnimatedTime;
    private float interpolationFraction;
    private Object animatedValue;

    private State status;
    private enum State {
        INIT,
        START,
        RUN,
        STOP
    }

    private boolean changeStatus(State state) {
        if (state == this.status) return false;

        this.status = state;
        return true;
    }

    private Animator() {
        this.status = State.INIT;
    }

    public static Animator ofObject(TypeEvaluator evaluator, Object startValue, Object endValue) {
        Animator anim = new Animator();
        anim.setObjectValues(startValue, endValue);
        anim.setEvaluator(evaluator);
        return anim;
    }

    public void setObjectValues(Object startValue, Object endValue) {
        //TODO: exception if called in-between is start and is finish
        this.startValue = startValue;
        this.endValue = endValue;
    }

    private void setEvaluator(TypeEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    public void setInterpolator(TimeInterpolator interpolator) {
        this.interpolator = interpolator;
    }

    public Animator setDuration(long duration) {
        if (duration < 0) {
            throw new IllegalArgumentException("Animators cannot have negative duration: " +
                    duration);
        }
        this.duration = duration;
        return this;
    }

    public void addUpdateListener(AnimatorListener listener) {
        this.updateListener = listener;
    }

    public void start(AnimatorManager animatorManager) {
        if (changeStatus(State.START)) {
            this.lastAnimatedTime = 0;

            this.interpolationFraction = 0;
            this.animatedValue = 0;

            animatorManager.addAnimation(this);

            if (!animatorManager.isStared()) {
                animatorManager.onStart();
            }
        }
    }

    public void cancel() {
        changeStatus(State.STOP);
    }

    public Object getAnimatedValue() {
        return this.animatedValue;
    }

    public float getAnimatedFraction() {
        return this.interpolationFraction;
    }

    public void animateBaseOnTime(long time) {
        if (this.status == State.START) updateListener.onAnimationStart(this);

        changeStatus(State.RUN);
        if (lastAnimatedTime == 0) {
            lastAnimatedTime = time;
        }
        float fraction = (float)(time - lastAnimatedTime) / duration;
        if (fraction > 1.0f) {
            fraction = 1.0f;
        }

        this.interpolationFraction = interpolator.getInterpolation(fraction);
        this.animatedValue = evaluator.evaluate(this.interpolationFraction, startValue, endValue);

        updateListener.onAnimationUpdate(this);

        if (this.interpolationFraction >= 1.0f) {
            this.interpolationFraction = 1.0f;
            if (changeStatus(State.STOP)) updateListener.onAnimationEnd(this);
        }
    }

    public boolean isFinish() {
        return this.status == State.STOP;
    }

    public static interface AnimatorListener {
        void onAnimationStart(Animator animator);

        void onAnimationUpdate(Animator animator);
        void onAnimationEnd(Animator animator);
    }
}
