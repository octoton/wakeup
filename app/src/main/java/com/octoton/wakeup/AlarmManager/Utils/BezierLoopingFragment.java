package com.octoton.wakeup.AlarmManager.Utils;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.ContentView;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public abstract class BezierLoopingFragment extends Fragment {

    String msg = "Bezier Looper : ";
    private Handler handler;
    private Runnable runnable;
    private final PathMeasure pathMeasure;
    private final int duration;
    private final float step;
    private float[] next_point;
    private float distance;
    private int delay;
    private float current_t;

    public BezierLoopingFragment(Path path, int duration, float step) {
        super();

        this.duration = duration;
        this.step = step;

        this.next_point = new float[2];
        this.pathMeasure = new PathMeasure(path, false);
        this.distance = 0;
    }

    @ContentView
    public BezierLoopingFragment(@LayoutRes int contentLayoutId, Path path, int duration, float step) {
        super(contentLayoutId);

        this.duration = duration;
        this.step = step;

        this.next_point = new float[2];
        this.pathMeasure = new PathMeasure(path, false);
        this.distance = 0;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onStart() {
        Log.d(msg, "The onStart() event");
        super.onStart();

        float length = pathMeasure.getLength();

        handler.postDelayed( runnable = new Runnable() {
            public void run() {
                action(next_point);

                // calculate next point
                current_t = next_point[0] * duration;
                distance += step * length;
                if (distance >= length) {
                    distance = length;
                }
                pathMeasure.getPosTan(distance, next_point, null);
                delay = (int) (next_point[0] * duration - current_t);

                Log.d(msg, "Delay: " + delay + " Time: " + current_t + " " + debugInfo());

                if (delay > 0.0) {
                    handler.postDelayed(runnable, delay);
                }
            }
        }, delay);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
        handler.removeCallbacks(runnable);
    }

    abstract public void action(float[] next_point);

    public String debugInfo() {
        return "abstract bezier looping fragment with no action";
    }
}
