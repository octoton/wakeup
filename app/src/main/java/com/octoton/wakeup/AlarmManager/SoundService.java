package com.octoton.wakeup.AlarmManager;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.animation.IntEvaluator;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.documentfile.provider.DocumentFile;

import com.octoton.wakeup.AlarmManager.Animator.Animator;
import com.octoton.wakeup.AlarmManager.Animator.AnimatorManager;
import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.RandomUtils;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;
import com.octoton.wakeup.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SoundService implements Module {
    static String msg = "Sound service : ";

    ArrayList<Integer> forbiddenIndex;

    private static final AtomicBoolean isReloading = new AtomicBoolean(false);
    private static final AtomicBoolean hasCompletion = new AtomicBoolean(false);

    Song song;
    MediaPlayer mp;
    public static final int MAX_VOLUME = 500;

    Animator soundAnimator;

    SoundData data;

    Context context;

    @Override
    public Integer getParentId() {
        return null;
    }

    private float getLogVolume(float volume) {
        if (volume == MAX_VOLUME) return 1.0f;
        return (float) (1.0f - Math.log(MAX_VOLUME - volume) / Math.log(MAX_VOLUME));
    }

    public void start(int position) {
        //float amount = Math.min((float) position / data.duration, 1.0f);
        soundAnimator = Animator.ofObject(new IntEvaluator(), data.startVolume, data.targetVolume);
        soundAnimator.setInterpolator(PathInterpolatorCompat.create(data.path));
        soundAnimator.setDuration(data.duration); // milliseconds
        soundAnimator.addUpdateListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationUpdate(Animator animator) {
                float log1 = getLogVolume((int) animator.getAnimatedValue());

                if (mp != null) // if sound stopped before animation could stop
                    mp.setVolume(log1, log1); // TODO: if alarm sound volume is mute, sound is really low, is volume is full blast sound is really high, seems like user doesn't have to chose inside the app            }
            }

            @Override
            public void onAnimationEnd(Animator animator) {}
        });
        soundAnimator.start(AnimatorManager.GetServiceAnimatorManager(null));

        if (mp == null || !hasCompletion.get()) {
            createMediaPlayer();
            startMediaPlayer();

            if (song.isFile) {
                mp.setOnCompletionListener(null);
                hasCompletion.set(false);
            } else {
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.i(msg, "onComplete hit");

                        synchronized (isReloading) {
                            stopMediaPlayer();
                            song.uri = getRandomFile(context, data.uri);

                            start(0);
                        }
                    }
                });
                hasCompletion.set(true);
            }
        }

        isReloading.set(false);
    }

    private void createMediaPlayer() {
        if (mp == null) {
            mp = new MediaPlayer();
            mp.setAudioAttributes(
                    new AudioAttributes.Builder()
                            .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                            .setLegacyStreamType(AudioManager.STREAM_ALARM)
                            .setUsage(AudioAttributes.USAGE_ALARM)
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build());
        }
        try {
            //mp.setDataSource(context, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM));
            mp.setDataSource(context, song.uri);
            //TODO: enable user to choose the song
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();

            try {
            Uri backupSong = new Uri.Builder()
                    .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                    .authority(context.getPackageName())
                    .appendPath(String.valueOf(R.raw.confusion_blip))
                    .build();
            mp.setDataSource(context, backupSong);
            mp.prepare();
            } catch (IOException ee) {
                ee.printStackTrace();
            }
        }
        Log.d(msg, "create mp");
    }

    private void startMediaPlayer() {
        float start = getLogVolume(data.startVolume);
        mp.setVolume(start, start);
        mp.setLooping(song.isFile);

        mp.start();
        Log.d(msg, "start mp");
    }

    private void stopMediaPlayer() {
        if (mp != null) {
            Log.d(msg, "stop mp");

            mp.stop();
            mp.reset();
            mp = null;
        }
    }

    public void stop() {
        stopMediaPlayer();

        //TODO: soundANimation stop ??
        soundAnimator.cancel();

        Log.d(msg, "On stop");
    }

    @Override
    public void activityRegisteredCLick() {

    }

    public boolean isDocumentTreeUri(Uri uri) {
        return "content".equalsIgnoreCase(uri.getScheme()) &&
                "com.android.externalstorage.documents".equalsIgnoreCase(uri.getAuthority());
    }
    public boolean isFolderUri(Uri uri, Context context) {
        Log.d(msg, "test folder");
        if (!isDocumentTreeUri(uri)) { Log.d(msg, "nope"); return false; }
        DocumentFile documentFile = DocumentFile.fromTreeUri(context, uri);
        return documentFile != null && documentFile.isDirectory();
    }

    public static void askForPermission(Activity activity, boolean needSoundInMemory) {
        if (!needSoundInMemory) { return; }

        /*private val readImagePermission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            Manifest.permission.READ_MEDIA_IMAGES
        else Manifest.permission.READ_EXTERNAL_STORAGE*/
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_MEDIA_AUDIO}, 1);
        }

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        }
    }

    private Uri getRandomFile(Context context, Uri uri) {
        Log.d(msg, "folder");
        DocumentFile documentFile = DocumentFile.fromTreeUri(context, uri);
        DocumentFile[] files = documentFile.listFiles();
        Log.d("Files", "Size: "+ files.length);

        int randomIndex = RandomUtils.nextIntBoundWithForbiddenNumber(files.length, forbiddenIndex);
        forbiddenIndex.add(randomIndex);
        Set<Integer> forbiddenIntegerSet = new TreeSet<>(forbiddenIndex);
        if (forbiddenIntegerSet.size() >= files.length) {
            forbiddenIndex = new ArrayList<>(); // all songs where played, now we can play them again
        }

        return files[randomIndex].getUri();
    }

    public static SoundData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        SoundData data = new SoundData();

        data.name = SoundData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        data.startVolume = sharedPreferences.getInt(SoundData.STARTVOLUME, 0);
        data.targetVolume = sharedPreferences.getInt(SoundData.TARGETVOLUME, MAX_VOLUME);

        String backupSong = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(context.getPackageName())
                .appendPath(String.valueOf(R.raw.confusion_blip))
                .build().toString();
        String path = sharedPreferences.getString(SoundData.SONG, backupSong);
        data.uri = Uri.parse(path);

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);

        forbiddenIndex = new ArrayList<>();
        song = new Song();
        this.context = context;

        synchronized (isReloading) {
            Log.d(msg, "load URI: " + this.data.uri);
            Log.d(msg, "load SCHEME: " + this.data.uri.getScheme());
            if (!isFolderUri(this.data.uri, context)) {
                Log.d(msg, "file");
                song.uri = this.data.uri;
                song.isFile = true;
            } else {
                Log.d(msg, "folder");
                song.uri = getRandomFile(context, data.uri);
                song.isFile = false;
            }
        }
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
        //Module.write(editor, data);
        editor.putInt(SoundData.STARTVOLUME, ((SoundData)data).startVolume);
        editor.putInt(SoundData.TARGETVOLUME, ((SoundData)data).targetVolume);

        editor.putString(SoundData.SONG, ((SoundData)data).uri.toString());
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step)
    {
        Log.d(msg, "reload");
        isReloading.set(true);

        if (data.transition == TRANSITION.HARD) {
            Log.d(msg, "hard");
            stop();
            load(context, sharedPreferences, alarmModel, moduleModel);

            //start(step.currentDuration); //TODO: redundant, as start is called after reload
        } else {
            Log.d(msg, "soft");
            load(context, sharedPreferences, alarmModel, moduleModel);
        }
        data.transition = TRANSITION.WAIT_FOR_END; //Patch: make alarm file to folder work for now as first step song is a file
    }

    public Data getData() {
        return data;
    }

    private static class Song {
        public Uri uri;
        public boolean isFile;

        public Song() {}
    }

    public enum TRANSITION {
        HARD,
        CROSSFADE,
        WAIT_FOR_END;
    }

    public static class SoundData extends Module.Data {
        public static String SHARED_PREFERENCE = "sound_data_shared-preference";
        public static String SONG = "song";
        public static String STARTVOLUME = "start volume";
        public static String TARGETVOLUME = "target volume";

        public Uri uri;
        public int startVolume;
        public int targetVolume;

        public TRANSITION transition;

        public SoundData() {
            this.transition = TRANSITION.HARD;
        }
        public SoundData(Uri uri, int startVolume, int targetVolume) {
            this.uri = uri;

            if (startVolume <= 0) { this.startVolume = 1; }
            else if (startVolume > MAX_VOLUME) { this.startVolume = MAX_VOLUME; }
            else { this.startVolume = startVolume; }

            if (targetVolume <= 0) { this.targetVolume = 1; }
            else if (targetVolume > MAX_VOLUME) { this.targetVolume = MAX_VOLUME; }
            else { this.targetVolume = targetVolume; }
        }

        public boolean songIsRaw(Activity activity) {
            return uri.toString().contains(activity.getPackageName());
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            SoundData data = (SoundData) object;
            if (!this.uri.equals(data.uri)) return false;
            if (this.startVolume != data.startVolume) return false;
            if (this.targetVolume != data.targetVolume) return false;

            return true;
        }
    }
}
