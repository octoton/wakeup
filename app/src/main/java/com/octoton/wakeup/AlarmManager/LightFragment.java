package com.octoton.wakeup.AlarmManager;

import static com.octoton.wakeup.AlarmManager.Utils.BrightnessUtils.convertGammaToLinear;
import static com.octoton.wakeup.AlarmManager.Utils.BrightnessUtils.convertLinearToGamma;

import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.Settings;

import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.fragment.app.Fragment;

import com.octoton.wakeup.AlarmManager.Module.Module;
import com.octoton.wakeup.AlarmManager.Utils.Step;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.ModuleModel;

public class LightFragment extends Fragment implements Module {
    String msg = "Light Fragment : ";
    int brightness;
    public static int MAX_BRIGHTNESS = 255;
    int default_Brightness = MAX_BRIGHTNESS / 2;
    int default_Brightness_Mode = Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL;
    ValueAnimator lightAnimation;

    LightData data;
    float amount;

    public LightFragment() {
        super();
    }

    @Override
    public Integer getParentId() {
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

        int brightnessMode;
        try {
            brightnessMode = Settings.System.getInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
            if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                Settings.System.putInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            }

            default_Brightness_Mode = Settings.System.getInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
            default_Brightness = Settings.System.getInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);

        } catch (Settings.SettingNotFoundException e) {
            throw new RuntimeException(e);
        }

        run();
    }

    @Override
    public void onStop() {
        super.onStop();

        stop();

        // TODO: when crash on sound_fragment start this doesn't seems to be called even tho light as already been change
        Settings.System.putInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, default_Brightness);
        Settings.System.putInt(requireContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, default_Brightness_Mode);
    }

    @Override
    public void activityRegisteredCLick() {

    }

    private void run() {
        int startGamma = convertLinearToGamma(data.startBrightness, 0, MAX_BRIGHTNESS);
        int targetGamma = convertLinearToGamma(data.targetBrightness, 0, MAX_BRIGHTNESS);

        lightAnimation = ValueAnimator.ofObject(new IntEvaluator(), startGamma, targetGamma);
        lightAnimation.setInterpolator(PathInterpolatorCompat.create(data.path));
        lightAnimation.setDuration(data.duration); // milliseconds
        lightAnimation.setCurrentFraction(amount);
        lightAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (getContext() == null) {
                    return;
                }

                // All thanks to https://www.stkent.com/2018/11/12/more-on-android-pies-brightness-control-changes.html
                int value = convertGammaToLinear((int) animator.getAnimatedValue(), 0, MAX_BRIGHTNESS);
                if (value <= 0) {
                    value = 1;
                } else if (value >= 255) {
                    value = 255;
                }
                //Log.d(msg, "Tick: "+(int) animator.getAnimatedValue()+" Light perceived: "+value);
                Settings.System.putInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, value);
            }

        });
        lightAnimation.start();
    }

    @Override
    public void start(int position) {
        amount = Math.min((float) position / data.duration, 1.0f);
    }

    @Override
    public void stop() {
        lightAnimation.end();
    }

    @Override
    public void reload(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel, Step step)
    {
        stop();
        load(context, sharedPreferences, alarmModel, moduleModel);
        start(step.currentDuration);
        run();
    }

    public static LightData loadData(Context context, SharedPreferences sharedPreferences, ModuleModel moduleModel) {
        LightData data = new LightData();

        data.name = LightData.SHARED_PREFERENCE;
        data.duration = moduleModel.getDuration();
        data.step_number = moduleModel.getStepNumber();
        data.path = moduleModel.getActivationCurve().getPath();

        data.startBrightness = sharedPreferences.getInt(LightData.STARTBRIGHTNESS, 0);
        data.targetBrightness = sharedPreferences.getInt(LightData.TARGETBRIGHTNESS, MAX_BRIGHTNESS);

        return data;
    }

    @Override
    public void load(Context context, SharedPreferences sharedPreferences, AlarmModel alarmModel, ModuleModel moduleModel) {
        this.data = loadData(context, sharedPreferences, moduleModel);

        brightness = data.startBrightness;
    }

    public static void askForPermission(Activity activity) {
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        // TODO: only ask that when user needs it (thus when in alarm profile setting, user ask for light management
        if (!Settings.System.canWrite(activity)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + activity.getPackageName()));
            activity.startActivity(intent);
        }
        //}
    }

    public static void write(SharedPreferences.Editor editor, Data data) {
        //Module.write(editor, data);
        editor.putInt(LightData.STARTBRIGHTNESS, ((LightData)data).startBrightness);
        editor.putInt(LightData.TARGETBRIGHTNESS, ((LightData)data).targetBrightness);
    }

    public Data getData() {
        return data;
    }

    public static class LightData extends Module.Data {
        public static String SHARED_PREFERENCE = "light_data_shared-preference";
        public static String STARTBRIGHTNESS = "start brightness";
        public static String TARGETBRIGHTNESS = "target brightness";

        public int startBrightness;
        public int targetBrightness;

        public LightData() {}
        public LightData(int startBrightness, int targetBrightness) {
            if (startBrightness <= 0) { this.startBrightness = 1; }
            else if (startBrightness > MAX_BRIGHTNESS) { this.startBrightness = MAX_BRIGHTNESS; }
            else { this.startBrightness = startBrightness; }

            if (targetBrightness <= 0) { this.targetBrightness = 1; }
            else if (targetBrightness > MAX_BRIGHTNESS) { this.targetBrightness = MAX_BRIGHTNESS; }
            else { this.targetBrightness = targetBrightness; }
        }

        @Override
        public boolean equals(Object object) {
            if (object == null) return false;
            if (getClass() != object.getClass()) return false;

            LightData data = (LightData) object;
            if (this.startBrightness != data.startBrightness) return false;
            if (this.targetBrightness != data.targetBrightness) return false;

            return true;
        }
    }
}
