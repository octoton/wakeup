package com.octoton.wakeup.Preference;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.WindowCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.octoton.wakeup.R;

public class SettingsActivity extends AppCompatActivity {
    public SettingsActivity() {
        super(R.layout.activity_preference);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapseLayout);
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitle("Settings");

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_preference, new SettingsFragment())
                .commit();

        Activity activity = this;
        findViewById(R.id.back).setOnClickListener(view -> activity.finish());
    }
}

