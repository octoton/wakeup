package com.octoton.wakeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.octoton.wakeup.Database.AlarmDatabase;
import com.octoton.wakeup.Database.AlarmModel;
import com.octoton.wakeup.Database.AlarmModelDao;
import com.octoton.wakeup.Notification.AlarmNotification;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BootReceiver extends BroadcastReceiver { // Only for boot or else will need android.permission.SYSTEM_ALERT_WINDOW
    String msg = "Boot Broadcast : ";

    // TODO: move to external class with below onreceiver executor
    public static void activateAlarms(AlarmModelDao alarmModelDao, Context context) {
        List<AlarmModel> alarmModels = alarmModelDao.getAllAlarmItemsSynchronous();
        for (AlarmModel alarmModel : alarmModels) {
            if (alarmModel.getIsActivated()) {
                AlarmNotification.setAlarmOrNextIfIgnored(context, alarmModel);
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(msg, "start");
        // Thanks to https://medium.com/@hrithik481/implement-a-perfect-alarm-in-android-3424efa2775b
        if ((Intent.ACTION_BOOT_COMPLETED).equals(intent.getAction())) {
            // reset all alarms
            AlarmDatabase db = AlarmDatabase.getDatabase(context);
            AlarmModelDao alarmModelDao = db.alarmModel();
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> activateAlarms(alarmModelDao, context));
        }
    }
}
