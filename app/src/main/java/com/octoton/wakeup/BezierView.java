package com.octoton.wakeup;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.util.Log;
import androidx.annotation.Nullable;

public class BezierView extends View {
    String msg = "Bezier : ";
    Paint paint1;
    Paint paint2;
    Paint paint3;
    Path path;

    PathMeasure pathMeasure;

    public BezierView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BezierView(Context context) {
        super(context);
        init();
        invalidate();
    }

    public void init() {
        paint1 = new Paint();
        paint2 = new Paint();
        paint3 = new Paint();
        path = new Path();

        // curve line
        paint1.setStyle(Paint.Style.STROKE);
        paint1.setARGB(255, 3, 106, 218);
        paint1.setAntiAlias(true);

        // tangent line
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setARGB(255, 3, 218, 197);
        paint2.setAntiAlias(true);

        // point
        paint3.setStyle(Paint.Style.FILL);
        paint3.setARGB(255, 3, 106, 218);
    }

    public static float anc0X = 0.6f;
    public static float anc0Y = 0.06f;
    public static float anc1X = -0.6f;
    public static float anc1Y = -0.4f;
    /*public static float anc0X = 0.8f;
    public static float anc0Y = 0.08f;
    public static float anc1X = -0.5f;
    public static float anc1Y = -0.8f;*/
    /*public static float anc0X = 0.1f;
    public static float anc0Y = 0.1f;
    public static float anc1X = -0.1f;
    public static float anc1Y = -0.1f;*/
    public static void defineCurve(Path path, int width, int height) {
        path.moveTo(0, 0);
        //path.cubicTo(anc0X*width, anc0Y*height, (anc1X+1.0f) * width, (anc1Y+1.0f) * height, width, height);
        path.cubicTo(anc0X*width, anc0Y*height, (anc1X+1.0f) * width, (anc1Y+1.0f) * height, width, height);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //canvas.save();
        //canvas.rotate(270, (int)(this.getWidth()/2), (int)(this.getHeight()/2));
        canvas.scale(1f, -1f, this.getWidth() / 2f, this.getHeight() / 2f);

        defineCurve(path, this.getWidth(), this.getHeight());

        pathMeasure = new PathMeasure(path, false);
        float[] next_point = new float[2]; next_point[0] = 0; next_point[1] = 0;
        float current = next_point[0];
        float distance = 0;
        while (distance < pathMeasure.getLength()) {
            canvas.drawCircle(next_point[0], next_point[1], 4, paint3);
            Log.d(msg, "Delay(s): "+(next_point[0] - current)+" Time(s): "+next_point[0]+" Volume: "+next_point[1]);

            current = next_point[0];
            distance += 0.09f * pathMeasure.getLength();
            pathMeasure.getPosTan(distance, next_point, null);
        }
        /*Matrix mMatrix = new Matrix();
        RectF bounds = new RectF();
        path.computeBounds(bounds, true);
        mMatrix.postRotate(-90, bounds.centerX(), bounds.centerY());
        path.transform(mMatrix);*/
        //path.moveTo(X0, Y0);
        //path.cubicTo(anc0X+X0, anc0Y+Y0, anc1X+X1, anc1Y+Y1, X1, Y1);
        canvas.drawPath(path, paint1);

        canvas.drawLine(0*this.getWidth(), 0*this.getHeight(), (anc0X+0)*this.getWidth(), (anc0Y+0)*this.getHeight(), paint2);
        canvas.drawLine(1*this.getWidth(), 1*this.getHeight(), (anc1X+1)*this.getWidth(), (anc1Y+1)*this.getHeight(), paint2);

        canvas.drawCircle((anc0X+0)*this.getWidth(), (anc0Y+0)*this.getHeight(), 6, paint2);
        canvas.drawCircle((anc1X+1)*this.getWidth(), (anc1Y+1)*this.getHeight(), 6, paint2);
    }
}
