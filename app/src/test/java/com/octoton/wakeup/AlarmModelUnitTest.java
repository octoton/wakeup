package com.octoton.wakeup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.octoton.wakeup.Database.AlarmModel;
import java.util.Calendar;

@PowerMockIgnore("jdk.internal.reflect.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AlarmModel.class } )
public class AlarmModelUnitTest {
    @Test
    public void alarm_IsNextDayOk() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.getFirstDayOfWeek();

        assertEquals(2, AlarmModel.getDay(day, 0));
        assertEquals(3, AlarmModel.getDay(day, 1));
        assertEquals(4, AlarmModel.getDay(day, 2));
        assertEquals(5, AlarmModel.getDay(day, 3));
        assertEquals(6, AlarmModel.getDay(day, 4));
        assertEquals(7, AlarmModel.getDay(day, 5));
        assertEquals(1, AlarmModel.getDay(day, 6));
    }

    @Test
    public void alarm_IsConvertOk() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.getFirstDayOfWeek();

        AlarmModel test = new AlarmModel("test", 12, 12, 0, true);
        assertEquals(AlarmModel.DAY.MONDAY, test.convertDay(day));
    }

    @Test
    public void alarm_IsActivatedOk() {
        AlarmModel test = new AlarmModel("test", 12, 12, 0b1100100, true);
        assertTrue(test.isDayActivated(AlarmModel.DAY.MONDAY));
        assertTrue(test.isDayActivated(AlarmModel.DAY.TUESDAY));
        assertFalse(test.isDayActivated(AlarmModel.DAY.WEDNESDAY));
        assertFalse(test.isDayActivated(AlarmModel.DAY.THURSDAY));
        assertTrue(test.isDayActivated(AlarmModel.DAY.FRIDAY));
        assertFalse(test.isDayActivated(AlarmModel.DAY.SATURDAY));
        assertFalse(test.isDayActivated(AlarmModel.DAY.SUNDAY));
    }

    @Test
    public void alarm_IsHowMuchDayOk() {
        AlarmModel test = new AlarmModel("test", 12, 12, 0b1101000, true);
        assertEquals(1, test.inHowMuchDayIsNextActivatedDay(1)); // Sunday
        assertEquals(1, test.inHowMuchDayIsNextActivatedDay(2)); // Monday
        assertEquals(2, test.inHowMuchDayIsNextActivatedDay(3)); // ...
        assertEquals(1, test.inHowMuchDayIsNextActivatedDay(4));
        assertEquals(4, test.inHowMuchDayIsNextActivatedDay(5));
        assertEquals(3, test.inHowMuchDayIsNextActivatedDay(6));
        assertEquals(2, test.inHowMuchDayIsNextActivatedDay(7));
    }

    @Test
    public void alarm_IsGetCalendarOk() {
        Calendar mockedCalendar = Calendar.getInstance();
        mockedCalendar.set(Calendar.YEAR, 2024);
        mockedCalendar.set(Calendar.MONTH, Calendar.APRIL);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 26);
        mockedCalendar.set(Calendar.HOUR_OF_DAY, 20);
        mockedCalendar.set(Calendar.MINUTE, 20);
        mockedCalendar.set(Calendar.SECOND, 0);
        mockedCalendar.set(Calendar.MILLISECOND, 0);

        Calendar today = (Calendar) mockedCalendar.clone();

        PowerMockito.mockStatic(Calendar.class);
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone());

        AlarmModel test = new AlarmModel("test", 20, 20, 0b1101100, true);
        Calendar result = test.getCalendar();
        assertEquals(mockedCalendar, result);

        test.setDay(AlarmModel.DAY.FRIDAY, false);
        result = test.getCalendar();

        mockedCalendar.set(Calendar.DAY_OF_MONTH, 29);
        assertEquals(mockedCalendar, result);
    }
    @Test
    public void alarm_IsGetCalendar2Ok() {
        Calendar mockedCalendar = Calendar.getInstance();
        mockedCalendar.set(Calendar.YEAR, 2024);
        mockedCalendar.set(Calendar.MONTH, Calendar.APRIL);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 26);
        mockedCalendar.set(Calendar.HOUR_OF_DAY, 18);
        mockedCalendar.set(Calendar.MINUTE, 20);
        mockedCalendar.set(Calendar.SECOND, 0);
        mockedCalendar.set(Calendar.MILLISECOND, 0);

        Calendar today = (Calendar) mockedCalendar.clone();

        PowerMockito.mockStatic(Calendar.class);
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone());
        AlarmModel test = new AlarmModel("test", 21, 40, 0b1111100, true);

        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getCalendar, to ensure no pointer strangeness
        Calendar result = test.getCalendar();
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getNextCalendar
        Calendar resultNext = test.getNextCalendar();

        mockedCalendar.set(Calendar.HOUR_OF_DAY, 21);
        mockedCalendar.set(Calendar.MINUTE, 40);
        assertEquals(mockedCalendar, result);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 29);
        assertEquals(mockedCalendar, resultNext);

        mockedCalendar.set(Calendar.DAY_OF_MONTH, 26);
        test.setDay(AlarmModel.DAY.FRIDAY, false);
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getCalendar
        result = test.getCalendar();
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getNextCalendar
        resultNext = test.getNextCalendar();

        mockedCalendar.set(Calendar.DAY_OF_MONTH, 29);
        assertEquals(mockedCalendar, result);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 30);
        assertEquals(mockedCalendar, resultNext);
    }
    @Test
    public void alarm_IsGetCalendar3Ok() {
        Calendar mockedCalendar = Calendar.getInstance();
        mockedCalendar.set(Calendar.YEAR, 2024);
        mockedCalendar.set(Calendar.MONTH, Calendar.APRIL);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 26);
        mockedCalendar.set(Calendar.HOUR_OF_DAY, 18);
        mockedCalendar.set(Calendar.MINUTE, 20);
        mockedCalendar.set(Calendar.SECOND, 0);
        mockedCalendar.set(Calendar.MILLISECOND, 0);

        Calendar today = (Calendar) mockedCalendar.clone();

        PowerMockito.mockStatic(Calendar.class);
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone());
        AlarmModel test = new AlarmModel("test", 21, 40, 0b1111111, true);

        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getCalendar, to ensure no pointer strangeness
        Calendar result = test.getCalendar();
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getNextCalendar
        Calendar resultNext = test.getNextCalendar();

        mockedCalendar.set(Calendar.HOUR_OF_DAY, 21);
        mockedCalendar.set(Calendar.MINUTE, 40);
        assertEquals(mockedCalendar, result);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 27);
        assertEquals(mockedCalendar, resultNext);

        mockedCalendar.set(Calendar.DAY_OF_MONTH, 26);
        test.setDay(AlarmModel.DAY.FRIDAY, false);
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getCalendar
        result = test.getCalendar();
        PowerMockito.when(Calendar.getInstance()).thenReturn((Calendar) today.clone(), (Calendar) today.clone(), (Calendar) today.clone()); //Same number of call done in getNextCalendar
        resultNext = test.getNextCalendar();

        mockedCalendar.set(Calendar.DAY_OF_MONTH, 27);
        assertEquals(mockedCalendar, result);
        mockedCalendar.set(Calendar.DAY_OF_MONTH, 28);
        assertEquals(mockedCalendar, resultNext);
    }
}
